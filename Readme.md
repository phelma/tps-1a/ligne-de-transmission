# TP Ligne de Transmission
https://gricad-gitlab.univ-grenoble-alpes.fr/phelma/tps-1a/ligne-de-transmission.git

Ce TP a pour but de présenter aux étudiants le fonctionnement des câbles coaxiaux.

Il est constitué de deux cartes électroniques, ligne et mesure.

L'explication physique de la ligne est dans le dossier documents.


## Maximum ratings

* La tension d'alimentation symétrique de la carte mesure est de ±10V.
  Il ne faut pas dépasser ±30V.

* La tension du signal RF au sein de la ligne ne doit pas dépasser ±5V (gamme des
  multiplexeurs analogiques).\
  Sachant que le gain maximal de la ligne est de ~7, il est préférable de rester
  en-deçà de ±1V en signal d'entrée.

## Électronique

### Boîtier Ligne
La carte Ligne est constituée de cellules LC, ainsi que des sample-holders qui
maintiennent les valeurs analogiques, qui sont récupérées plus tard par la carte
mesure.

Elle est connectée par :
* Deux connecteurs coaxiaux BNC
* Un connecteur "ligne" pour la carte de mesures
* 6 points de mesure en-ligne pour des prises banane.

Elle est (sera ?) dans un boîtier découpé à la CNC (laser ?), dont la face
supérieure pourra être transparente pour rendre visible le circuit aux étudiants.

Elle est manufacturée par ProtoProcess.
Pour générer les fichiers gerber/bom, un script est disponible ici : \
https://gist.github.com/Salamandar/7162bcb9f0eeb31028aaa052a779a025

#### BOM du boîtier :
Référence       | Quantité |  Description
---             |:---:     |:---
Gotronic 0829x  | 6 | Douilles banane colorées BE450J
RS 175-5643     | 6 | Boîtier JST-XH ×1
RS 820-1529     | 6 | Contact JST-XH

Process         | Quantité |  Description
---             |:---:     |:---
Usinage CNC     | 1 | `meca/ligne_boitier.scad`, épaisseur = 5mm


### Carte de mesure

Elle contient un système de multiplexeurs analogiques et un arduino qui renvoie
les données en UART par USB.

### BOM du boîtier :

Référence       | Quantité |  Description
---             |:---:     |:---
Gotronic 0829x  | 3 | Douilles banane colorées BE450 (1×N, 1×R, 1×B)

Process         | Quantité |  Description
---             |:---:     |:---
Usinage CNC     | 1 | TODO Boîtier


# TODOs
