EESchema Schematic File Version 4
LIBS:TP_LigneDeTransmission-cache
EELAYER 30 0
EELAYER END
$Descr A1 33110 23386
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR016
U 1 1 5C9BF0F8
P 8300 10000
AR Path="/5C9BF0F8" Ref="#PWR016"  Part="1" 
AR Path="/5C1AEE01/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5C9BF0F8" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C9BF0F8" Ref="#PWR?"  Part="1" 
F 0 "#PWR016" H 8300 9750 50  0001 C CNN
F 1 "GND" H 8305 9827 50  0000 C CNN
F 2 "" H 8300 10000 50  0001 C CNN
F 3 "" H 8300 10000 50  0001 C CNN
	1    8300 10000
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U5
U 1 1 5C9BF106
P 8150 8950
AR Path="/5C9BF106" Ref="U5"  Part="1" 
AR Path="/5C1AEE01/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5C9BF106" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C9BF106" Ref="U?"  Part="1" 
F 0 "U5" H 8050 9000 50  0000 C CNN
F 1 "SMP04" H 8500 9000 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 8950 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 8950 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 9200 7600 9200
Wire Wire Line
	7600 9200 7600 9400
Wire Wire Line
	7900 9400 7600 9400
Connection ~ 7600 9400
Wire Wire Line
	7600 9400 7600 9600
Wire Wire Line
	7600 9600 7900 9600
Connection ~ 7600 9600
Wire Wire Line
	7600 9600 7600 9800
Wire Wire Line
	7600 9800 7900 9800
$Comp
L power:GND #PWR018
U 1 1 5CA24F56
P 8300 11600
AR Path="/5CA24F56" Ref="#PWR018"  Part="1" 
AR Path="/5C1AEE01/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA24F56" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA24F56" Ref="#PWR?"  Part="1" 
F 0 "#PWR018" H 8300 11350 50  0001 C CNN
F 1 "GND" H 8305 11427 50  0000 C CNN
F 2 "" H 8300 11600 50  0001 C CNN
F 3 "" H 8300 11600 50  0001 C CNN
	1    8300 11600
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U6
U 1 1 5CA24F64
P 8150 10550
AR Path="/5CA24F64" Ref="U6"  Part="1" 
AR Path="/5C1AEE01/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA24F64" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA24F64" Ref="U?"  Part="1" 
F 0 "U6" H 8050 10600 50  0000 C CNN
F 1 "SMP04" H 8500 10600 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 10550 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 10550 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 10550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 10800 7600 10800
Wire Wire Line
	7600 10800 7600 11000
Wire Wire Line
	7900 11000 7600 11000
Connection ~ 7600 11000
Wire Wire Line
	7600 11000 7600 11200
Wire Wire Line
	7600 11200 7900 11200
Connection ~ 7600 11200
Wire Wire Line
	7600 11200 7600 11400
Wire Wire Line
	7600 11400 7900 11400
$Comp
L power:GND #PWR020
U 1 1 5CA2E035
P 8300 13200
AR Path="/5CA2E035" Ref="#PWR020"  Part="1" 
AR Path="/5C1AEE01/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA2E035" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA2E035" Ref="#PWR?"  Part="1" 
F 0 "#PWR020" H 8300 12950 50  0001 C CNN
F 1 "GND" H 8305 13027 50  0000 C CNN
F 2 "" H 8300 13200 50  0001 C CNN
F 3 "" H 8300 13200 50  0001 C CNN
	1    8300 13200
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U7
U 1 1 5CA2E043
P 8150 12150
AR Path="/5CA2E043" Ref="U7"  Part="1" 
AR Path="/5C1AEE01/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA2E043" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA2E043" Ref="U?"  Part="1" 
F 0 "U7" H 8050 12200 50  0000 C CNN
F 1 "SMP04" H 8500 12200 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 12150 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 12150 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 12150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 12400 7600 12400
Wire Wire Line
	7600 12400 7600 12600
Wire Wire Line
	7900 12600 7600 12600
Connection ~ 7600 12600
Wire Wire Line
	7600 12600 7600 12800
Wire Wire Line
	7600 12800 7900 12800
Connection ~ 7600 12800
Wire Wire Line
	7600 12800 7600 13000
Wire Wire Line
	7600 13000 7900 13000
$Comp
L power:GND #PWR022
U 1 1 5CA2E062
P 8300 14800
AR Path="/5CA2E062" Ref="#PWR022"  Part="1" 
AR Path="/5C1AEE01/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA2E062" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA2E062" Ref="#PWR?"  Part="1" 
F 0 "#PWR022" H 8300 14550 50  0001 C CNN
F 1 "GND" H 8305 14627 50  0000 C CNN
F 2 "" H 8300 14800 50  0001 C CNN
F 3 "" H 8300 14800 50  0001 C CNN
	1    8300 14800
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U8
U 1 1 5CA2E070
P 8150 13750
AR Path="/5CA2E070" Ref="U8"  Part="1" 
AR Path="/5C1AEE01/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA2E070" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA2E070" Ref="U?"  Part="1" 
F 0 "U8" H 8050 13800 50  0000 C CNN
F 1 "SMP04" H 8500 13800 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 13750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 13750 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 13750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 14000 7600 14000
Wire Wire Line
	7600 14000 7600 14200
Wire Wire Line
	7900 14200 7600 14200
Connection ~ 7600 14200
Wire Wire Line
	7600 14200 7600 14400
Wire Wire Line
	7600 14400 7900 14400
Connection ~ 7600 14400
Wire Wire Line
	7600 14400 7600 14600
Wire Wire Line
	7600 14600 7900 14600
$Comp
L power:GND #PWR05
U 1 1 5CA39A0D
P 8300 3600
AR Path="/5CA39A0D" Ref="#PWR05"  Part="1" 
AR Path="/5C1AEE01/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA39A0D" Ref="#PWR?"  Part="1" 
F 0 "#PWR05" H 8300 3350 50  0001 C CNN
F 1 "GND" H 8305 3427 50  0000 C CNN
F 2 "" H 8300 3600 50  0001 C CNN
F 3 "" H 8300 3600 50  0001 C CNN
	1    8300 3600
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U1
U 1 1 5CA39A1B
P 8150 2550
AR Path="/5CA39A1B" Ref="U1"  Part="1" 
AR Path="/5C1AEE01/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A1B" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA39A1B" Ref="U?"  Part="1" 
F 0 "U1" H 8050 2600 50  0000 C CNN
F 1 "SMP04" H 8500 2600 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 2550 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 2550 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2800 7600 2800
Wire Wire Line
	7600 2800 7600 3000
Wire Wire Line
	7900 3000 7600 3000
Connection ~ 7600 3000
Wire Wire Line
	7600 3000 7600 3200
Wire Wire Line
	7600 3200 7900 3200
Connection ~ 7600 3200
Wire Wire Line
	7600 3200 7600 3400
Wire Wire Line
	7600 3400 7900 3400
Text Label 7900 2700 2    50   ~ 0
mes1
Text Label 7900 2900 2    50   ~ 0
mes2
Text Label 7900 3100 2    50   ~ 0
mes3
Text Label 7900 3300 2    50   ~ 0
mes4
$Comp
L power:GND #PWR08
U 1 1 5CA39A3A
P 8300 5200
AR Path="/5CA39A3A" Ref="#PWR08"  Part="1" 
AR Path="/5C1AEE01/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A3A" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA39A3A" Ref="#PWR?"  Part="1" 
F 0 "#PWR08" H 8300 4950 50  0001 C CNN
F 1 "GND" H 8305 5027 50  0000 C CNN
F 2 "" H 8300 5200 50  0001 C CNN
F 3 "" H 8300 5200 50  0001 C CNN
	1    8300 5200
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U2
U 1 1 5CA39A48
P 8150 4150
AR Path="/5CA39A48" Ref="U2"  Part="1" 
AR Path="/5C1AEE01/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A48" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA39A48" Ref="U?"  Part="1" 
F 0 "U2" H 8050 4200 50  0000 C CNN
F 1 "SMP04" H 8500 4200 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 4150 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 4150 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4400 7600 4400
Wire Wire Line
	7600 4400 7600 4600
Wire Wire Line
	7900 4600 7600 4600
Connection ~ 7600 4600
Wire Wire Line
	7600 4600 7600 4800
Wire Wire Line
	7600 4800 7900 4800
Connection ~ 7600 4800
Wire Wire Line
	7600 4800 7600 5000
Wire Wire Line
	7600 5000 7900 5000
$Comp
L power:GND #PWR011
U 1 1 5CA39A67
P 8300 6800
AR Path="/5CA39A67" Ref="#PWR011"  Part="1" 
AR Path="/5C1AEE01/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A67" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA39A67" Ref="#PWR?"  Part="1" 
F 0 "#PWR011" H 8300 6550 50  0001 C CNN
F 1 "GND" H 8305 6627 50  0000 C CNN
F 2 "" H 8300 6800 50  0001 C CNN
F 3 "" H 8300 6800 50  0001 C CNN
	1    8300 6800
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U3
U 1 1 5CA39A75
P 8150 5750
AR Path="/5CA39A75" Ref="U3"  Part="1" 
AR Path="/5C1AEE01/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A75" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA39A75" Ref="U?"  Part="1" 
F 0 "U3" H 8050 5800 50  0000 C CNN
F 1 "SMP04" H 8500 5800 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 5750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 5750 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 6000 7600 6000
Wire Wire Line
	7600 6000 7600 6200
Wire Wire Line
	7900 6200 7600 6200
Connection ~ 7600 6200
Wire Wire Line
	7600 6200 7600 6400
Wire Wire Line
	7600 6400 7900 6400
Connection ~ 7600 6400
Wire Wire Line
	7600 6400 7600 6600
Wire Wire Line
	7600 6600 7900 6600
$Comp
L power:GND #PWR014
U 1 1 5CA39A94
P 8300 8400
AR Path="/5CA39A94" Ref="#PWR014"  Part="1" 
AR Path="/5C1AEE01/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39A94" Ref="#PWR?"  Part="1" 
AR Path="/5C1AA865/5CA39A94" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 8300 8150 50  0001 C CNN
F 1 "GND" H 8305 8227 50  0000 C CNN
F 2 "" H 8300 8400 50  0001 C CNN
F 3 "" H 8300 8400 50  0001 C CNN
	1    8300 8400
	1    0    0    -1  
$EndComp
$Comp
L SMP04:SMP04 U4
U 1 1 5CA39AA2
P 8150 7350
AR Path="/5CA39AA2" Ref="U4"  Part="1" 
AR Path="/5C1AEE01/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1B8C06/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1EBB42/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F1EC5/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C1F9233/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C200A3E/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C209418/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2124D5/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C21C13B/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C226C26/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2328D4/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C23F401/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C24C9A5/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C25C084/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C2FE807/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C30DB75/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C31CE81/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C32B921/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C33B55D/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C34ADC5/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C359865/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3689E9/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C377B0B/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3865AB/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3957F3/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5C3A4293/5CA39AA2" Ref="U?"  Part="1" 
AR Path="/5C1AA865/5CA39AA2" Ref="U?"  Part="1" 
F 0 "U4" H 8050 7400 50  0000 C CNN
F 1 "SMP04" H 8500 7400 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8150 7350 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/SMP04.pdf" H 8150 7350 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/echantillonneurs-bloqueurs/5236939/" H 0   0   50  0001 C CNN "Farnell"
F 5 "8,35" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "SMP04ESZ" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    8150 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 7600 7600 7600
Wire Wire Line
	7600 7600 7600 7800
Wire Wire Line
	7900 7800 7600 7800
Connection ~ 7600 7800
Wire Wire Line
	7600 7800 7600 8000
Wire Wire Line
	7600 8000 7900 8000
Connection ~ 7600 8000
Wire Wire Line
	7600 8000 7600 8200
Wire Wire Line
	7600 8200 7900 8200
Text Label 7900 4300 2    50   ~ 0
mes5
Text Label 7900 4500 2    50   ~ 0
mes6
Text Label 7900 4700 2    50   ~ 0
mes7
Text Label 7900 4900 2    50   ~ 0
mes8
Text Label 7900 5900 2    50   ~ 0
mes9
Text Label 7900 6100 2    50   ~ 0
mes10
Text Label 7900 6300 2    50   ~ 0
mes11
Text Label 7900 6500 2    50   ~ 0
mes12
Text Label 7900 7500 2    50   ~ 0
mes13
Text Label 7900 7700 2    50   ~ 0
mes14
Text Label 7900 7900 2    50   ~ 0
mes15
Text Label 7900 8100 2    50   ~ 0
mes16
Text Label 7900 9100 2    50   ~ 0
mes17
Text Label 7900 9300 2    50   ~ 0
mes18
Text Label 7900 9500 2    50   ~ 0
mes19
Text Label 7900 9700 2    50   ~ 0
mes20
Text Label 7900 10700 2    50   ~ 0
mes21
Text Label 7900 10900 2    50   ~ 0
mes22
Text Label 7900 11100 2    50   ~ 0
mes23
Text Label 7900 11300 2    50   ~ 0
mes24
Text Label 7900 12300 2    50   ~ 0
mes25
Text Label 7900 12500 2    50   ~ 0
mes26
Text Label 7900 12700 2    50   ~ 0
mes27
Text Label 7900 12900 2    50   ~ 0
mes28
Text Label 7900 13900 2    50   ~ 0
mes29
Text Label 7900 14100 2    50   ~ 0
mes30
Text Label 7900 14300 2    50   ~ 0
mes31
Text Label 7900 14500 2    50   ~ 0
mes32
Text Label 8800 2700 0    50   ~ 0
hold_out_1
Text Label 8800 2900 0    50   ~ 0
hold_out_2
Text Label 8800 3100 0    50   ~ 0
hold_out_3
Text Label 8800 3300 0    50   ~ 0
hold_out_4
Text Label 8800 4300 0    50   ~ 0
hold_out_5
Text Label 8800 4500 0    50   ~ 0
hold_out_6
Text Label 8800 4700 0    50   ~ 0
hold_out_7
Text Label 8800 4900 0    50   ~ 0
hold_out_8
Text Label 8800 5900 0    50   ~ 0
hold_out_9
Text Label 8800 6100 0    50   ~ 0
hold_out_10
Text Label 8800 6300 0    50   ~ 0
hold_out_11
Text Label 8800 6500 0    50   ~ 0
hold_out_12
Text Label 8800 7500 0    50   ~ 0
hold_out_13
Text Label 8800 7700 0    50   ~ 0
hold_out_14
Text Label 8800 7900 0    50   ~ 0
hold_out_15
Text Label 8800 8100 0    50   ~ 0
hold_out_16
Text Label 8800 9100 0    50   ~ 0
hold_out_17
Text Label 8800 9300 0    50   ~ 0
hold_out_18
Text Label 8800 9500 0    50   ~ 0
hold_out_19
Text Label 8800 9700 0    50   ~ 0
hold_out_20
Text Label 8800 10700 0    50   ~ 0
hold_out_21
Text Label 8800 10900 0    50   ~ 0
hold_out_22
Text Label 8800 11100 0    50   ~ 0
hold_out_23
Text Label 8800 11300 0    50   ~ 0
hold_out_24
Text Label 8800 12300 0    50   ~ 0
hold_out_25
Text Label 8800 12500 0    50   ~ 0
hold_out_26
Text Label 8800 12700 0    50   ~ 0
hold_out_27
Text Label 8800 12900 0    50   ~ 0
hold_out_28
Text Label 8800 13900 0    50   ~ 0
hold_out_29
Text Label 8800 14100 0    50   ~ 0
hold_out_30
Text Label 8800 14300 0    50   ~ 0
hold_out_31
Text Label 8800 14500 0    50   ~ 0
hold_out_32
Text Label 11500 3200 2    50   ~ 0
hold_out_1
Text Label 11500 3300 2    50   ~ 0
hold_out_3
Text Label 11500 3400 2    50   ~ 0
hold_out_5
Text Label 11500 3500 2    50   ~ 0
hold_out_7
Text Label 11500 3600 2    50   ~ 0
hold_out_9
Text Label 11500 3700 2    50   ~ 0
hold_out_11
Text Label 11500 3800 2    50   ~ 0
hold_out_13
Text Label 11500 3900 2    50   ~ 0
hold_out_15
Text Label 11500 4200 2    50   ~ 0
hold_out_17
Text Label 11500 4300 2    50   ~ 0
hold_out_19
Text Label 11500 4400 2    50   ~ 0
hold_out_21
Text Label 11500 4500 2    50   ~ 0
hold_out_23
Text Label 11500 4600 2    50   ~ 0
hold_out_25
Text Label 11500 4700 2    50   ~ 0
hold_out_27
Text Label 11500 4800 2    50   ~ 0
hold_out_29
Text Label 11500 4900 2    50   ~ 0
hold_out_31
Text Label 12000 3200 0    50   ~ 0
hold_out_2
Text Label 12000 3300 0    50   ~ 0
hold_out_4
Text Label 12000 3400 0    50   ~ 0
hold_out_6
Text Label 12000 3500 0    50   ~ 0
hold_out_8
Text Label 12000 3600 0    50   ~ 0
hold_out_10
Text Label 12000 3700 0    50   ~ 0
hold_out_12
Text Label 12000 3800 0    50   ~ 0
hold_out_14
Text Label 12000 3900 0    50   ~ 0
hold_out_16
Text Label 12000 4200 0    50   ~ 0
hold_out_18
Text Label 12000 4300 0    50   ~ 0
hold_out_20
Text Label 12000 4400 0    50   ~ 0
hold_out_22
Text Label 12000 4500 0    50   ~ 0
hold_out_24
Text Label 12000 4600 0    50   ~ 0
hold_out_26
Text Label 12000 4700 0    50   ~ 0
hold_out_28
Text Label 12000 4800 0    50   ~ 0
hold_out_30
Text Label 12000 4900 0    50   ~ 0
hold_out_32
$Comp
L power:GND #PWR07
U 1 1 5C38163F
P 12000 3100
F 0 "#PWR07" H 12000 2850 50  0001 C CNN
F 1 "GND" V 12000 3000 50  0000 R CNN
F 2 "" H 12000 3100 50  0001 C CNN
F 3 "" H 12000 3100 50  0001 C CNN
	1    12000 3100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5C383987
P 11500 5000
F 0 "#PWR010" H 11500 4750 50  0001 C CNN
F 1 "GND" V 11500 4900 50  0000 R CNN
F 2 "" H 11500 5000 50  0001 C CNN
F 3 "" H 11500 5000 50  0001 C CNN
	1    11500 5000
	0    1    1    0   
$EndComp
Text Label 8300 2450 0    50   ~ 0
Vline+
Text Label 8400 3600 0    50   ~ 0
Vline-
Text Label 8400 5200 0    50   ~ 0
Vline-
Text Label 8400 6800 0    50   ~ 0
Vline-
Text Label 8400 8400 0    50   ~ 0
Vline-
Text Label 8400 10000 0    50   ~ 0
Vline-
Text Label 8400 11600 0    50   ~ 0
Vline-
Text Label 8400 13200 0    50   ~ 0
Vline-
Text Label 8400 14800 0    50   ~ 0
Vline-
Text Label 8300 4050 0    50   ~ 0
Vline+
Text Label 8300 5650 0    50   ~ 0
Vline+
Text Label 8300 7250 0    50   ~ 0
Vline+
Text Label 8300 8850 0    50   ~ 0
Vline+
Text Label 8300 10450 0    50   ~ 0
Vline+
Text Label 8300 12050 0    50   ~ 0
Vline+
Text Label 8300 13650 0    50   ~ 0
Vline+
Text Label 11500 3100 2    50   ~ 0
Vline+
Text Label 12000 4100 0    50   ~ 0
Vline-
$Comp
L Device:C C68
U 1 1 5C415789
P 6550 3100
F 0 "C68" H 6665 3146 50  0000 L CNN
F 1 "100n" H 6665 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6588 2950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6550 3100 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6550 3100
	1    0    0    -1  
$EndComp
Text Label 6550 2950 0    50   ~ 0
Vline+
$Comp
L Device:C C67
U 1 1 5C4171B9
P 6100 3100
F 0 "C67" H 6215 3146 50  0000 L CNN
F 1 "100n" H 6215 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6138 2950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6100 3100 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6100 3100
	1    0    0    -1  
$EndComp
Text Label 6100 2950 0    50   ~ 0
Vline-
Wire Wire Line
	6100 3250 6300 3250
$Comp
L power:GND #PWR04
U 1 1 5C42491E
P 6300 3250
F 0 "#PWR04" H 6300 3000 50  0001 C CNN
F 1 "GND" H 6305 3077 50  0000 C CNN
F 2 "" H 6300 3250 50  0001 C CNN
F 3 "" H 6300 3250 50  0001 C CNN
	1    6300 3250
	1    0    0    -1  
$EndComp
Connection ~ 6300 3250
Wire Wire Line
	6300 3250 6550 3250
$Comp
L Device:C C70
U 1 1 5C4253E2
P 6600 4550
F 0 "C70" H 6715 4596 50  0000 L CNN
F 1 "100n" H 6715 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6638 4400 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6600 4550 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6600 4550
	1    0    0    -1  
$EndComp
Text Label 6600 4400 0    50   ~ 0
Vline+
$Comp
L Device:C C69
U 1 1 5C4253EA
P 6150 4550
F 0 "C69" H 6265 4596 50  0000 L CNN
F 1 "100n" H 6265 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6188 4400 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6150 4550 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6150 4550
	1    0    0    -1  
$EndComp
Text Label 6150 4400 0    50   ~ 0
Vline-
Wire Wire Line
	6150 4700 6350 4700
$Comp
L power:GND #PWR06
U 1 1 5C4253F3
P 6350 4700
F 0 "#PWR06" H 6350 4450 50  0001 C CNN
F 1 "GND" H 6355 4527 50  0000 C CNN
F 2 "" H 6350 4700 50  0001 C CNN
F 3 "" H 6350 4700 50  0001 C CNN
	1    6350 4700
	1    0    0    -1  
$EndComp
Connection ~ 6350 4700
Wire Wire Line
	6350 4700 6600 4700
$Comp
L Device:C C72
U 1 1 5C4333B8
P 6700 6200
F 0 "C72" H 6815 6246 50  0000 L CNN
F 1 "100n" H 6815 6155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6738 6050 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6700 6200 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6700 6200
	1    0    0    -1  
$EndComp
Text Label 6700 6050 0    50   ~ 0
Vline+
$Comp
L Device:C C71
U 1 1 5C4333C0
P 6250 6200
F 0 "C71" H 6365 6246 50  0000 L CNN
F 1 "100n" H 6365 6155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6288 6050 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6250 6200 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6250 6200
	1    0    0    -1  
$EndComp
Text Label 6250 6050 0    50   ~ 0
Vline-
Wire Wire Line
	6250 6350 6450 6350
$Comp
L power:GND #PWR09
U 1 1 5C4333C9
P 6450 6350
F 0 "#PWR09" H 6450 6100 50  0001 C CNN
F 1 "GND" H 6455 6177 50  0000 C CNN
F 2 "" H 6450 6350 50  0001 C CNN
F 3 "" H 6450 6350 50  0001 C CNN
	1    6450 6350
	1    0    0    -1  
$EndComp
Connection ~ 6450 6350
Wire Wire Line
	6450 6350 6700 6350
$Comp
L Device:C C74
U 1 1 5C4422C1
P 6650 7950
F 0 "C74" H 6765 7996 50  0000 L CNN
F 1 "100n" H 6765 7905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6688 7800 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6650 7950 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6650 7950
	1    0    0    -1  
$EndComp
Text Label 6650 7800 0    50   ~ 0
Vline+
$Comp
L Device:C C73
U 1 1 5C4422C9
P 6200 7950
F 0 "C73" H 6315 7996 50  0000 L CNN
F 1 "100n" H 6315 7905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6238 7800 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6200 7950 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6200 7950
	1    0    0    -1  
$EndComp
Text Label 6200 7800 0    50   ~ 0
Vline-
Wire Wire Line
	6200 8100 6400 8100
$Comp
L power:GND #PWR013
U 1 1 5C4422D2
P 6400 8100
F 0 "#PWR013" H 6400 7850 50  0001 C CNN
F 1 "GND" H 6405 7927 50  0000 C CNN
F 2 "" H 6400 8100 50  0001 C CNN
F 3 "" H 6400 8100 50  0001 C CNN
	1    6400 8100
	1    0    0    -1  
$EndComp
Connection ~ 6400 8100
Wire Wire Line
	6400 8100 6650 8100
$Comp
L Device:C C76
U 1 1 5C4510B4
P 6750 9450
F 0 "C76" H 6865 9496 50  0000 L CNN
F 1 "100n" H 6865 9405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6788 9300 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6750 9450 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6750 9450
	1    0    0    -1  
$EndComp
Text Label 6750 9300 0    50   ~ 0
Vline+
$Comp
L Device:C C75
U 1 1 5C4510BC
P 6300 9450
F 0 "C75" H 6415 9496 50  0000 L CNN
F 1 "100n" H 6415 9405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6338 9300 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6300 9450 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6300 9450
	1    0    0    -1  
$EndComp
Text Label 6300 9300 0    50   ~ 0
Vline-
Wire Wire Line
	6300 9600 6500 9600
$Comp
L power:GND #PWR015
U 1 1 5C4510C5
P 6500 9600
F 0 "#PWR015" H 6500 9350 50  0001 C CNN
F 1 "GND" H 6505 9427 50  0000 C CNN
F 2 "" H 6500 9600 50  0001 C CNN
F 3 "" H 6500 9600 50  0001 C CNN
	1    6500 9600
	1    0    0    -1  
$EndComp
Connection ~ 6500 9600
Wire Wire Line
	6500 9600 6750 9600
$Comp
L Device:C C78
U 1 1 5C4605A9
P 6550 11150
F 0 "C78" H 6665 11196 50  0000 L CNN
F 1 "100n" H 6665 11105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6588 11000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6550 11150 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6550 11150
	1    0    0    -1  
$EndComp
Text Label 6550 11000 0    50   ~ 0
Vline+
$Comp
L Device:C C77
U 1 1 5C4605B1
P 6100 11150
F 0 "C77" H 6215 11196 50  0000 L CNN
F 1 "100n" H 6215 11105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6138 11000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6100 11150 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6100 11150
	1    0    0    -1  
$EndComp
Text Label 6100 11000 0    50   ~ 0
Vline-
Wire Wire Line
	6100 11300 6300 11300
$Comp
L power:GND #PWR017
U 1 1 5C4605BA
P 6300 11300
F 0 "#PWR017" H 6300 11050 50  0001 C CNN
F 1 "GND" H 6305 11127 50  0000 C CNN
F 2 "" H 6300 11300 50  0001 C CNN
F 3 "" H 6300 11300 50  0001 C CNN
	1    6300 11300
	1    0    0    -1  
$EndComp
Connection ~ 6300 11300
Wire Wire Line
	6300 11300 6550 11300
$Comp
L Device:C C80
U 1 1 5C470B3F
P 6650 12650
F 0 "C80" H 6765 12696 50  0000 L CNN
F 1 "100n" H 6765 12605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6688 12500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6650 12650 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6650 12650
	1    0    0    -1  
$EndComp
Text Label 6650 12500 0    50   ~ 0
Vline+
$Comp
L Device:C C79
U 1 1 5C470B47
P 6200 12650
F 0 "C79" H 6315 12696 50  0000 L CNN
F 1 "100n" H 6315 12605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6238 12500 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6200 12650 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6200 12650
	1    0    0    -1  
$EndComp
Text Label 6200 12500 0    50   ~ 0
Vline-
Wire Wire Line
	6200 12800 6400 12800
$Comp
L power:GND #PWR019
U 1 1 5C470B50
P 6400 12800
F 0 "#PWR019" H 6400 12550 50  0001 C CNN
F 1 "GND" H 6405 12627 50  0000 C CNN
F 2 "" H 6400 12800 50  0001 C CNN
F 3 "" H 6400 12800 50  0001 C CNN
	1    6400 12800
	1    0    0    -1  
$EndComp
Connection ~ 6400 12800
Wire Wire Line
	6400 12800 6650 12800
$Comp
L Device:C C82
U 1 1 5C480EE1
P 6700 14250
F 0 "C82" H 6815 14296 50  0000 L CNN
F 1 "100n" H 6815 14205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6738 14100 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6700 14250 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6700 14250
	1    0    0    -1  
$EndComp
Text Label 6700 14100 0    50   ~ 0
Vline+
$Comp
L Device:C C81
U 1 1 5C480EE9
P 6250 14250
F 0 "C81" H 6365 14296 50  0000 L CNN
F 1 "100n" H 6365 14205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6288 14100 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6250 14250 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj0805y104kxbtw1bc/cond-0-1-f-100v-10-x7r-0805/dp/2896560" H 0   0   50  0001 C CNN "Farnell"
F 5 "0,0696" H 0   0   50  0001 C CNN "Prix Unit"
F 6 "VJ0805Y104KXBTW1BC" H 0   0   50  0001 C CNN "Ref Fabricant"
	1    6250 14250
	1    0    0    -1  
$EndComp
Text Label 6250 14100 0    50   ~ 0
Vline-
Wire Wire Line
	6250 14400 6450 14400
$Comp
L power:GND #PWR021
U 1 1 5C480EF2
P 6450 14400
F 0 "#PWR021" H 6450 14150 50  0001 C CNN
F 1 "GND" H 6455 14227 50  0000 C CNN
F 2 "" H 6450 14400 50  0001 C CNN
F 3 "" H 6450 14400 50  0001 C CNN
	1    6450 14400
	1    0    0    -1  
$EndComp
Connection ~ 6450 14400
Wire Wire Line
	6450 14400 6700 14400
$Comp
L Device:C C64
U 1 1 5C4929AD
P 6550 1350
F 0 "C64" H 6665 1396 50  0000 L CNN
F 1 "1u" H 6665 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6588 1200 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6550 1350 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206y105kxxtw1bc/condensateur-1-f-25v-10-x7r-1206/dp/2896651" H -5500 250 50  0001 C CNN "Farnell"
F 5 "0,121" H -5500 250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206Y105KXXTW1BC" H -5500 250 50  0001 C CNN "Ref Fabricant"
	1    6550 1350
	1    0    0    -1  
$EndComp
Text Label 6550 1200 0    50   ~ 0
Vline+
$Comp
L Device:C C63
U 1 1 5C4929B5
P 6100 1350
F 0 "C63" H 6215 1396 50  0000 L CNN
F 1 "1u" H 6215 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6138 1200 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6100 1350 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206y105kxxtw1bc/condensateur-1-f-25v-10-x7r-1206/dp/2896651" H -5500 250 50  0001 C CNN "Farnell"
F 5 "0,121" H -5500 250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206Y105KXXTW1BC" H -5500 250 50  0001 C CNN "Ref Fabricant"
	1    6100 1350
	1    0    0    -1  
$EndComp
Text Label 6100 1200 0    50   ~ 0
Vline-
Wire Wire Line
	6100 1500 6300 1500
$Comp
L power:GND #PWR02
U 1 1 5C4929BE
P 6300 1500
F 0 "#PWR02" H 6300 1250 50  0001 C CNN
F 1 "GND" H 6305 1327 50  0000 C CNN
F 2 "" H 6300 1500 50  0001 C CNN
F 3 "" H 6300 1500 50  0001 C CNN
	1    6300 1500
	1    0    0    -1  
$EndComp
Connection ~ 6300 1500
Wire Wire Line
	6300 1500 6550 1500
$Comp
L Device:C C66
U 1 1 5C4A3A72
P 6550 2150
F 0 "C66" H 6665 2196 50  0000 L CNN
F 1 "1u" H 6665 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6588 2000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6550 2150 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206y105kxxtw1bc/condensateur-1-f-25v-10-x7r-1206/dp/2896651" H -5550 -200 50  0001 C CNN "Farnell"
F 5 "0,121" H -5550 -200 50  0001 C CNN "Prix Unit"
F 6 "VJ1206Y105KXXTW1BC" H -5550 -200 50  0001 C CNN "Ref Fabricant"
	1    6550 2150
	1    0    0    -1  
$EndComp
Text Label 6550 2000 0    50   ~ 0
Vline+
$Comp
L Device:C C65
U 1 1 5C4A3A7A
P 6100 2150
F 0 "C65" H 6215 2196 50  0000 L CNN
F 1 "1u" H 6215 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6138 2000 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6100 2150 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206y105kxxtw1bc/condensateur-1-f-25v-10-x7r-1206/dp/2896651" H -5550 -200 50  0001 C CNN "Farnell"
F 5 "0,121" H -5550 -200 50  0001 C CNN "Prix Unit"
F 6 "VJ1206Y105KXXTW1BC" H -5550 -200 50  0001 C CNN "Ref Fabricant"
	1    6100 2150
	1    0    0    -1  
$EndComp
Text Label 6100 2000 0    50   ~ 0
Vline-
Wire Wire Line
	6100 2300 6300 2300
$Comp
L power:GND #PWR03
U 1 1 5C4A3A83
P 6300 2300
F 0 "#PWR03" H 6300 2050 50  0001 C CNN
F 1 "GND" H 6305 2127 50  0000 C CNN
F 2 "" H 6300 2300 50  0001 C CNN
F 3 "" H 6300 2300 50  0001 C CNN
	1    6300 2300
	1    0    0    -1  
$EndComp
Connection ~ 6300 2300
Wire Wire Line
	6300 2300 6550 2300
Text Label 12000 5000 0    50   ~ 0
Vline+
Text Label 11500 4000 2    50   ~ 0
Vline-
Text Notes 7400 2050 0    50   ~ 0
SMP04\nVDD to DGND . . . . . . . . . . . . . . . . . . . . . . . . . . . –0.3 V, 17 V\nVDD to VSS . . . . . . . . . . . . . . . . . . . . . . . . . . . . –0.7 V, 17 V\nVSS + 0.05V ≤ Vout ≤ VDD – 2V\nNote that several specifications, including acquisition time,\noffset and output voltage compliance will degrade for a total\nsupply voltage of less than 7 V
$Comp
L Connector:TestPoint_Flag TP1
U 1 1 5C5DBA46
P 11500 6300
F 0 "TP1" H 11759 6396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 6305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 6300 50  0001 C CNN
F 3 "~" H 11700 6300 50  0001 C CNN
F 4 "0" H 11500 6300 50  0001 C CNN "Prix Unit"
	1    11500 6300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP2
U 1 1 5C5EEF5E
P 11500 6500
F 0 "TP2" H 11759 6596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 6505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 6500 50  0001 C CNN
F 3 "~" H 11700 6500 50  0001 C CNN
F 4 "0" H 11500 6500 50  0001 C CNN "Prix Unit"
	1    11500 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP3
U 1 1 5C5EF0BC
P 11500 6700
F 0 "TP3" H 11759 6796 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 6705 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 6700 50  0001 C CNN
F 3 "~" H 11700 6700 50  0001 C CNN
F 4 "0" H 11500 6700 50  0001 C CNN "Prix Unit"
	1    11500 6700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP4
U 1 1 5C612EE0
P 11500 6900
F 0 "TP4" H 11759 6996 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 6905 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 6900 50  0001 C CNN
F 3 "~" H 11700 6900 50  0001 C CNN
F 4 "0" H 11500 6900 50  0001 C CNN "Prix Unit"
	1    11500 6900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP5
U 1 1 5C612EE7
P 11500 7100
F 0 "TP5" H 11759 7196 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 7105 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 7100 50  0001 C CNN
F 3 "~" H 11700 7100 50  0001 C CNN
F 4 "0" H 11500 7100 50  0001 C CNN "Prix Unit"
	1    11500 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP6
U 1 1 5C612EEE
P 11500 7300
F 0 "TP6" H 11759 7396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 7305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 7300 50  0001 C CNN
F 3 "~" H 11700 7300 50  0001 C CNN
F 4 "0" H 11500 7300 50  0001 C CNN "Prix Unit"
	1    11500 7300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP7
U 1 1 5C6244D5
P 11500 7500
F 0 "TP7" H 11759 7596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 7505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 7500 50  0001 C CNN
F 3 "~" H 11700 7500 50  0001 C CNN
F 4 "0" H 11500 7500 50  0001 C CNN "Prix Unit"
	1    11500 7500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP8
U 1 1 5C6244DC
P 11500 7700
F 0 "TP8" H 11759 7796 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 7705 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 7700 50  0001 C CNN
F 3 "~" H 11700 7700 50  0001 C CNN
F 4 "0" H 11500 7700 50  0001 C CNN "Prix Unit"
	1    11500 7700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP9
U 1 1 5C6244E3
P 11500 7900
F 0 "TP9" H 11759 7996 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 7905 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 7900 50  0001 C CNN
F 3 "~" H 11700 7900 50  0001 C CNN
F 4 "0" H 11500 7900 50  0001 C CNN "Prix Unit"
	1    11500 7900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP10
U 1 1 5C636B2B
P 11500 8100
F 0 "TP10" H 11759 8196 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 8105 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 8100 50  0001 C CNN
F 3 "~" H 11700 8100 50  0001 C CNN
F 4 "0" H 11500 8100 50  0001 C CNN "Prix Unit"
	1    11500 8100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP11
U 1 1 5C636B32
P 11500 8300
F 0 "TP11" H 11759 8396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 8305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 8300 50  0001 C CNN
F 3 "~" H 11700 8300 50  0001 C CNN
F 4 "0" H 11500 8300 50  0001 C CNN "Prix Unit"
	1    11500 8300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP12
U 1 1 5C636B39
P 11500 8500
F 0 "TP12" H 11759 8596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 8505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 8500 50  0001 C CNN
F 3 "~" H 11700 8500 50  0001 C CNN
F 4 "0" H 11500 8500 50  0001 C CNN "Prix Unit"
	1    11500 8500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP13
U 1 1 5C6495A6
P 11500 8700
F 0 "TP13" H 11759 8796 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 8705 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 8700 50  0001 C CNN
F 3 "~" H 11700 8700 50  0001 C CNN
F 4 "0" H 11500 8700 50  0001 C CNN "Prix Unit"
	1    11500 8700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP14
U 1 1 5C6495AD
P 11500 8900
F 0 "TP14" H 11759 8996 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 8905 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 8900 50  0001 C CNN
F 3 "~" H 11700 8900 50  0001 C CNN
F 4 "0" H 11500 8900 50  0001 C CNN "Prix Unit"
	1    11500 8900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP15
U 1 1 5C6495B4
P 11500 9100
F 0 "TP15" H 11759 9196 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 9105 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 9100 50  0001 C CNN
F 3 "~" H 11700 9100 50  0001 C CNN
F 4 "0" H 11500 9100 50  0001 C CNN "Prix Unit"
	1    11500 9100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP16
U 1 1 5C6495BB
P 11500 9300
F 0 "TP16" H 11759 9396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 9305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 9300 50  0001 C CNN
F 3 "~" H 11700 9300 50  0001 C CNN
F 4 "0" H 11500 9300 50  0001 C CNN "Prix Unit"
	1    11500 9300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP17
U 1 1 5C6495C2
P 11500 9500
F 0 "TP17" H 11759 9596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 9505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 9500 50  0001 C CNN
F 3 "~" H 11700 9500 50  0001 C CNN
F 4 "0" H 11500 9500 50  0001 C CNN "Prix Unit"
	1    11500 9500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP18
U 1 1 5C6495C9
P 11500 9700
F 0 "TP18" H 11759 9796 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 9705 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 9700 50  0001 C CNN
F 3 "~" H 11700 9700 50  0001 C CNN
F 4 "0" H 11500 9700 50  0001 C CNN "Prix Unit"
	1    11500 9700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP19
U 1 1 5C6495D0
P 11500 9900
F 0 "TP19" H 11759 9996 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 9905 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 9900 50  0001 C CNN
F 3 "~" H 11700 9900 50  0001 C CNN
F 4 "0" H 11500 9900 50  0001 C CNN "Prix Unit"
	1    11500 9900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP20
U 1 1 5C6495D7
P 11500 10100
F 0 "TP20" H 11759 10196 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 10105 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 10100 50  0001 C CNN
F 3 "~" H 11700 10100 50  0001 C CNN
F 4 "0" H 11500 10100 50  0001 C CNN "Prix Unit"
	1    11500 10100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP21
U 1 1 5C6495DE
P 11500 10300
F 0 "TP21" H 11759 10396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 10305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 10300 50  0001 C CNN
F 3 "~" H 11700 10300 50  0001 C CNN
F 4 "0" H 11500 10300 50  0001 C CNN "Prix Unit"
	1    11500 10300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP22
U 1 1 5C6495E5
P 11500 10500
F 0 "TP22" H 11759 10596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 10505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 10500 50  0001 C CNN
F 3 "~" H 11700 10500 50  0001 C CNN
F 4 "0" H 11500 10500 50  0001 C CNN "Prix Unit"
	1    11500 10500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP23
U 1 1 5C6495EC
P 11500 10700
F 0 "TP23" H 11759 10796 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 10705 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 10700 50  0001 C CNN
F 3 "~" H 11700 10700 50  0001 C CNN
F 4 "0" H 11500 10700 50  0001 C CNN "Prix Unit"
	1    11500 10700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP25
U 1 1 5C6495F3
P 11500 11100
F 0 "TP25" H 11759 11196 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 11105 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 11100 50  0001 C CNN
F 3 "~" H 11700 11100 50  0001 C CNN
F 4 "0" H 11500 11100 50  0001 C CNN "Prix Unit"
	1    11500 11100
	1    0    0    -1  
$EndComp
Text Label 11500 6300 2    50   ~ 0
mes1
Text Label 11500 6500 2    50   ~ 0
mes2
Text Label 11500 6700 2    50   ~ 0
mes3
Text Label 11500 6900 2    50   ~ 0
mes4
Text Label 11500 7100 2    50   ~ 0
mes5
Text Label 11500 7300 2    50   ~ 0
mes6
Text Label 11500 7500 2    50   ~ 0
mes7
Text Label 11500 7700 2    50   ~ 0
mes8
Text Label 11500 7900 2    50   ~ 0
mes9
Text Label 11500 8100 2    50   ~ 0
mes10
Text Label 11500 8300 2    50   ~ 0
mes11
Text Label 11500 8500 2    50   ~ 0
mes12
Text Label 11500 8700 2    50   ~ 0
mes13
Text Label 11500 8900 2    50   ~ 0
mes14
Text Label 11500 9100 2    50   ~ 0
mes15
Text Label 11500 9300 2    50   ~ 0
mes16
Text Label 11500 9500 2    50   ~ 0
mes17
Text Label 11500 9700 2    50   ~ 0
mes18
Text Label 11500 9900 2    50   ~ 0
mes19
Text Label 11500 10100 2    50   ~ 0
mes20
Text Label 11500 10300 2    50   ~ 0
mes21
Text Label 11500 10500 2    50   ~ 0
mes22
Text Label 11500 10700 2    50   ~ 0
mes23
Text Label 11500 10900 2    50   ~ 0
mes24
Text Label 11500 11100 2    50   ~ 0
mes25
Text Label 11500 11300 2    50   ~ 0
mes26
Text Label 11500 11500 2    50   ~ 0
mes27
Text Label 11500 11700 2    50   ~ 0
mes28
Text Label 11500 11900 2    50   ~ 0
mes29
Text Label 11500 12100 2    50   ~ 0
mes30
Text Label 11500 12300 2    50   ~ 0
mes31
$Comp
L Connector:TestPoint_Flag TP24
U 1 1 5C693297
P 11500 10900
F 0 "TP24" H 11759 10996 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 10905 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 10900 50  0001 C CNN
F 3 "~" H 11700 10900 50  0001 C CNN
F 4 "0" H 11500 10900 50  0001 C CNN "Prix Unit"
	1    11500 10900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP26
U 1 1 5C693413
P 11500 11300
F 0 "TP26" H 11759 11396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 11305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 11300 50  0001 C CNN
F 3 "~" H 11700 11300 50  0001 C CNN
F 4 "0" H 11500 11300 50  0001 C CNN "Prix Unit"
	1    11500 11300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP27
U 1 1 5C693591
P 11500 11500
F 0 "TP27" H 11759 11596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 11505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 11500 50  0001 C CNN
F 3 "~" H 11700 11500 50  0001 C CNN
F 4 "0" H 11500 11500 50  0001 C CNN "Prix Unit"
	1    11500 11500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP28
U 1 1 5C693715
P 11500 11700
F 0 "TP28" H 11759 11796 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 11705 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 11700 50  0001 C CNN
F 3 "~" H 11700 11700 50  0001 C CNN
F 4 "0" H 11500 11700 50  0001 C CNN "Prix Unit"
	1    11500 11700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP29
U 1 1 5C693897
P 11500 11900
F 0 "TP29" H 11759 11996 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 11905 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 11900 50  0001 C CNN
F 3 "~" H 11700 11900 50  0001 C CNN
F 4 "0" H 11500 11900 50  0001 C CNN "Prix Unit"
	1    11500 11900
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP30
U 1 1 5C693A1B
P 11500 12100
F 0 "TP30" H 11759 12196 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 12105 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 12100 50  0001 C CNN
F 3 "~" H 11700 12100 50  0001 C CNN
F 4 "0" H 11500 12100 50  0001 C CNN "Prix Unit"
	1    11500 12100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP31
U 1 1 5C693BA9
P 11500 12300
F 0 "TP31" H 11759 12396 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 12305 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 12300 50  0001 C CNN
F 3 "~" H 11700 12300 50  0001 C CNN
F 4 "0" H 11500 12300 50  0001 C CNN "Prix Unit"
	1    11500 12300
	1    0    0    -1  
$EndComp
Text Label 11500 12500 2    50   ~ 0
mes32
$Comp
L Connector:TestPoint_Flag TP32
U 1 1 5C6B1860
P 11500 12500
F 0 "TP32" H 11759 12596 50  0000 L CNN
F 1 "TestPoint_Flag" H 11759 12505 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11700 12500 50  0001 C CNN
F 3 "~" H 11700 12500 50  0001 C CNN
F 4 "0" H 11500 12500 50  0001 C CNN "Prix Unit"
	1    11500 12500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5C5D50F7
P 30100 6000
F 0 "H1" V 30054 6150 50  0000 L CNN
F 1 "MountingHole_Pad" V 30145 6150 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 30100 6000 50  0001 C CNN
F 3 "~" H 30100 6000 50  0001 C CNN
F 4 "~" H 30100 6000 50  0001 C CNN "Farnell"
F 5 "0" H 30100 6000 50  0001 C CNN "Prix Unit"
	1    30100 6000
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5C5D5EBE
P 30100 6300
F 0 "H2" V 30054 6450 50  0000 L CNN
F 1 "MountingHole_Pad" V 30145 6450 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 30100 6300 50  0001 C CNN
F 3 "~" H 30100 6300 50  0001 C CNN
F 4 "~" H 30100 6300 50  0001 C CNN "Farnell"
F 5 "0" H 30100 6300 50  0001 C CNN "Prix Unit"
	1    30100 6300
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5C5D621B
P 30100 6600
F 0 "H3" V 30054 6750 50  0000 L CNN
F 1 "MountingHole_Pad" V 30145 6750 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 30100 6600 50  0001 C CNN
F 3 "~" H 30100 6600 50  0001 C CNN
F 4 "~" H 30100 6600 50  0001 C CNN "Farnell"
F 5 "0" H 30100 6600 50  0001 C CNN "Prix Unit"
	1    30100 6600
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5C5D6222
P 30100 6900
F 0 "H4" V 30054 7050 50  0000 L CNN
F 1 "MountingHole_Pad" V 30145 7050 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 30100 6900 50  0001 C CNN
F 3 "~" H 30100 6900 50  0001 C CNN
F 4 "~" H 30100 6900 50  0001 C CNN "Farnell"
F 5 "0" H 30100 6900 50  0001 C CNN "Prix Unit"
	1    30100 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	30000 6000 30000 6300
Connection ~ 30000 6300
Wire Wire Line
	30000 6300 30000 6600
Connection ~ 30000 6600
Wire Wire Line
	30000 6600 30000 6900
Connection ~ 30000 6900
Wire Wire Line
	30000 6900 30000 7150
$Comp
L power:GND #PWR012
U 1 1 5C5F8E09
P 30000 7150
F 0 "#PWR012" H 30000 6900 50  0001 C CNN
F 1 "GND" H 30005 6977 50  0000 C CNN
F 2 "" H 30000 7150 50  0001 C CNN
F 3 "" H 30000 7150 50  0001 C CNN
	1    30000 7150
	1    0    0    -1  
$EndComp
Text Label 11500 13000 2    50   ~ 0
TestPoint1
Text Label 11500 13200 2    50   ~ 0
TestPoint2
Text Label 11500 13400 2    50   ~ 0
TestPoint3
Text Label 11500 13600 2    50   ~ 0
TestPoint4
Text Label 11500 13800 2    50   ~ 0
TestPoint5
Text Label 11500 14000 2    50   ~ 0
TestPoint6
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5DCD398E
P 11700 13000
F 0 "J4" H 11780 13042 50  0000 L CNN
F 1 "Conn_01x01" H 11780 12951 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B1B-XH-AM_1x01_P2.50mm_Vertical" H 11700 13000 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654e60.pdf" H 11700 13000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1755473/" H 11700 13000 50  0001 C CNN "Farnell"
F 5 "0,148" H 11700 13000 50  0001 C CNN "Prix Unit"
F 6 "B1B-XH-AM (LF)(SN)" H 11700 13000 50  0001 C CNN "Ref Fabricant"
	1    11700 13000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5DCD453E
P 11700 13200
F 0 "J5" H 11780 13242 50  0000 L CNN
F 1 "Conn_01x01" H 11780 13151 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B1B-XH-AM_1x01_P2.50mm_Vertical" H 11700 13200 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654e60.pdf" H 11700 13200 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1755473/" H 11700 13200 50  0001 C CNN "Farnell"
F 5 "0,148" H 11700 13200 50  0001 C CNN "Prix Unit"
F 6 "B1B-XH-AM (LF)(SN)" H 11700 13200 50  0001 C CNN "Ref Fabricant"
	1    11700 13200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 5DCD46C7
P 11700 13400
F 0 "J6" H 11780 13442 50  0000 L CNN
F 1 "Conn_01x01" H 11780 13351 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B1B-XH-AM_1x01_P2.50mm_Vertical" H 11700 13400 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654e60.pdf" H 11700 13400 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1755473/" H 11700 13400 50  0001 C CNN "Farnell"
F 5 "0,148" H 11700 13400 50  0001 C CNN "Prix Unit"
F 6 "B1B-XH-AM (LF)(SN)" H 11700 13400 50  0001 C CNN "Ref Fabricant"
	1    11700 13400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 5DCD48E8
P 11700 13600
F 0 "J7" H 11780 13642 50  0000 L CNN
F 1 "Conn_01x01" H 11780 13551 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B1B-XH-AM_1x01_P2.50mm_Vertical" H 11700 13600 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654e60.pdf" H 11700 13600 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1755473/" H 11700 13600 50  0001 C CNN "Farnell"
F 5 "0,148" H 11700 13600 50  0001 C CNN "Prix Unit"
F 6 "B1B-XH-AM (LF)(SN)" H 11700 13600 50  0001 C CNN "Ref Fabricant"
	1    11700 13600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 5DCD4C19
P 11700 13800
F 0 "J8" H 11780 13842 50  0000 L CNN
F 1 "Conn_01x01" H 11780 13751 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B1B-XH-AM_1x01_P2.50mm_Vertical" H 11700 13800 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654e60.pdf" H 11700 13800 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1755473/" H 11700 13800 50  0001 C CNN "Farnell"
F 5 "0,148" H 11700 13800 50  0001 C CNN "Prix Unit"
F 6 "B1B-XH-AM (LF)(SN)" H 11700 13800 50  0001 C CNN "Ref Fabricant"
	1    11700 13800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 5DCD4D48
P 11700 14000
F 0 "J9" H 11780 14042 50  0000 L CNN
F 1 "Conn_01x01" H 11780 13951 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B1B-XH-AM_1x01_P2.50mm_Vertical" H 11700 14000 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1654/0900766b81654e60.pdf" H 11700 14000 50  0001 C CNN
F 4 "https://fr.rs-online.com/web/p/embases-circuits-imprimes/1755473/" H 11700 14000 50  0001 C CNN "Farnell"
F 5 "0,148" H 11700 14000 50  0001 C CNN "Prix Unit"
F 6 "B1B-XH-AM (LF)(SN)" H 11700 14000 50  0001 C CNN "Ref Fabricant"
	1    11700 14000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J3
U 1 1 5C6AC3AA
P 11700 4100
F 0 "J3" H 11750 5200 50  0000 C CNN
F 1 "Conn_Ligne_out" H 11750 5100 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x20_P2.54mm_Horizontal" H 11700 4100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1648843.pdf" H 11700 4100 50  0001 C CNN
F 4 "https://fr.farnell.com/amphenol/t821140a1r100ceu/embase-coudee-2-54mm-40voies/dp/2215300" H -350 -1550 50  0001 C CNN "Farnell"
F 5 "0,616" H -350 -1550 50  0001 C CNN "Prix Unit"
F 6 "T821140A1R100CEU" H -350 -1550 50  0001 C CNN "Ref Fabricant"
	1    11700 4100
	1    0    0    1   
$EndComp
$Comp
L Device:L L1
U 1 1 5C1A13EC
P 1300 16900
F 0 "L1" V 1400 16950 50  0000 C CNN
F 1 "1mH" V 1250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 1300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 1300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    1300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C1
U 1 1 5C1A140E
P 1150 17050
F 0 "C1" H 1050 17150 50  0000 L CNN
F 1 "1nF" H 1050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 1150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    1150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L2
U 1 1 5C1A13EA
P 1800 16900
F 0 "L2" V 1900 16950 50  0000 C CNN
F 1 "1mH" V 1750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 1800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 1800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    1800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C2
U 1 1 5C1A140D
P 1650 17050
F 0 "C2" H 1550 17150 50  0000 L CNN
F 1 "1nF" H 1550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 1650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    1650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L3
U 1 1 5C1A13E4
P 2300 16900
F 0 "L3" V 2400 16950 50  0000 C CNN
F 1 "1mH" V 2250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 2300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 2300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    2300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 5C1A140C
P 2150 17050
F 0 "C3" H 2050 17150 50  0000 L CNN
F 1 "1nF" H 2050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 2150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    2150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L4
U 1 1 5C1A13E0
P 2800 16900
F 0 "L4" V 2900 16950 50  0000 C CNN
F 1 "1mH" V 2750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 2800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 2800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    2800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 5C1A140B
P 2650 17050
F 0 "C4" H 2550 17150 50  0000 L CNN
F 1 "1nF" H 2550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 2650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    2650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L5
U 1 1 5C1A13DB
P 3300 16900
F 0 "L5" V 3400 16950 50  0000 C CNN
F 1 "1mH" V 3250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 3300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 3300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    3300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 5C1A140A
P 3150 17050
F 0 "C5" H 3050 17150 50  0000 L CNN
F 1 "1nF" H 3050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 3150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    3150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L6
U 1 1 5C1A13DC
P 3800 16900
F 0 "L6" V 3900 16950 50  0000 C CNN
F 1 "1mH" V 3750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 3800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 3800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    3800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C6
U 1 1 5C1A1409
P 3650 17050
F 0 "C6" H 3550 17150 50  0000 L CNN
F 1 "1nF" H 3550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 3650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    3650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L7
U 1 1 5C1A13DF
P 4300 16900
F 0 "L7" V 4400 16950 50  0000 C CNN
F 1 "1mH" V 4250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 4300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 4300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    4300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C7
U 1 1 5C1A1408
P 4150 17050
F 0 "C7" H 4050 17150 50  0000 L CNN
F 1 "1nF" H 4050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 4150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    4150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L8
U 1 1 5C1A13E1
P 4800 16900
F 0 "L8" V 4900 16950 50  0000 C CNN
F 1 "1mH" V 4750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 4800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 4800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    4800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C8
U 1 1 5C1A13FE
P 4650 17050
F 0 "C8" H 4550 17150 50  0000 L CNN
F 1 "1nF" H 4550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 4650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    4650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L9
U 1 1 5C1A13E3
P 5300 16900
F 0 "L9" V 5400 16950 50  0000 C CNN
F 1 "1mH" V 5250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 5300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 5300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    5300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C9
U 1 1 5C1A1406
P 5150 17050
F 0 "C9" H 5050 17150 50  0000 L CNN
F 1 "1nF" H 5050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 5150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    5150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L10
U 1 1 5C1A13DA
P 5800 16900
F 0 "L10" V 5900 16950 50  0000 C CNN
F 1 "1mH" V 5750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 5800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 5800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    5800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C10
U 1 1 5C1A1405
P 5650 17050
F 0 "C10" H 5550 17150 50  0000 L CNN
F 1 "1nF" H 5550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 5650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    5650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L11
U 1 1 5C1A13E6
P 6300 16900
F 0 "L11" V 6400 16950 50  0000 C CNN
F 1 "1mH" V 6250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 6300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 6300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    6300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C11
U 1 1 5C1A1404
P 6150 17050
F 0 "C11" H 6050 17150 50  0000 L CNN
F 1 "1nF" H 6050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    6150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L12
U 1 1 5C1A13E8
P 6800 16900
F 0 "L12" V 6900 16950 50  0000 C CNN
F 1 "1mH" V 6750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 6800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 6800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    6800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C12
U 1 1 5C1A1403
P 6650 17050
F 0 "C12" H 6550 17150 50  0000 L CNN
F 1 "1nF" H 6550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 6650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    6650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L13
U 1 1 5C1A13EB
P 7300 16900
F 0 "L13" V 7400 16950 50  0000 C CNN
F 1 "1mH" V 7250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 7300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 7300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    7300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C13
U 1 1 5C1A1402
P 7150 17050
F 0 "C13" H 7050 17150 50  0000 L CNN
F 1 "1nF" H 7050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 7150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    7150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L14
U 1 1 5C1A13ED
P 7800 16900
F 0 "L14" V 7900 16950 50  0000 C CNN
F 1 "1mH" V 7750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 7800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 7800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    7800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C14
U 1 1 5C1A1401
P 7650 17050
F 0 "C14" H 7550 17150 50  0000 L CNN
F 1 "1nF" H 7550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 7650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    7650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L15
U 1 1 5C1A13BF
P 8300 16900
F 0 "L15" V 8400 16950 50  0000 C CNN
F 1 "1mH" V 8250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 8300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 8300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    8300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C15
U 1 1 5C1A1400
P 8150 17050
F 0 "C15" H 8050 17150 50  0000 L CNN
F 1 "1nF" H 8050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 8150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    8150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L16
U 1 1 5C1A13CA
P 8800 16900
F 0 "L16" V 8900 16950 50  0000 C CNN
F 1 "1mH" V 8750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 8800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 8800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    8800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C16
U 1 1 5C1A13FF
P 8650 17050
F 0 "C16" H 8550 17150 50  0000 L CNN
F 1 "1nF" H 8550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 8650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    8650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L17
U 1 1 5C1A13C8
P 9300 16900
F 0 "L17" V 9400 16950 50  0000 C CNN
F 1 "1mH" V 9250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 9300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 9300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    9300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C17
U 1 1 5C1A1407
P 9150 17050
F 0 "C17" H 9050 17150 50  0000 L CNN
F 1 "1nF" H 9050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 9150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    9150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L18
U 1 1 5C1A13C7
P 9800 16900
F 0 "L18" V 9900 16950 50  0000 C CNN
F 1 "1mH" V 9750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 9800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 9800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    9800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C18
U 1 1 5C1A1410
P 9650 17050
F 0 "C18" H 9550 17150 50  0000 L CNN
F 1 "1nF" H 9550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 9650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    9650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L19
U 1 1 5C1A13C5
P 10300 16900
F 0 "L19" V 10400 16950 50  0000 C CNN
F 1 "1mH" V 10250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 10300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 10300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    10300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C19
U 1 1 5C1A1411
P 10150 17050
F 0 "C19" H 10050 17150 50  0000 L CNN
F 1 "1nF" H 10050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 10150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    10150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L20
U 1 1 5C1A13C4
P 10800 16900
F 0 "L20" V 10900 16950 50  0000 C CNN
F 1 "1mH" V 10750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 10800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 10800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    10800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C20
U 1 1 5C1A1412
P 10650 17050
F 0 "C20" H 10550 17150 50  0000 L CNN
F 1 "1nF" H 10550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 10650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    10650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L21
U 1 1 5C1A13C2
P 11300 16900
F 0 "L21" V 11400 16950 50  0000 C CNN
F 1 "1mH" V 11250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 11300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 11300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    11300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C21
U 1 1 5C1A1413
P 11150 17050
F 0 "C21" H 11050 17150 50  0000 L CNN
F 1 "1nF" H 11050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 11188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 11150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    11150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L22
U 1 1 5C1A13C1
P 11800 16900
F 0 "L22" V 11900 16950 50  0000 C CNN
F 1 "1mH" V 11750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 11800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 11800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    11800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C22
U 1 1 5C1A1414
P 11650 17050
F 0 "C22" H 11550 17150 50  0000 L CNN
F 1 "1nF" H 11550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 11688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 11650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    11650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L23
U 1 1 5C1A13BE
P 12300 16900
F 0 "L23" V 12400 16950 50  0000 C CNN
F 1 "1mH" V 12250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 12300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 12300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    12300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C23
U 1 1 5C1A1415
P 12150 17050
F 0 "C23" H 12050 17150 50  0000 L CNN
F 1 "1nF" H 12050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 12188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 12150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    12150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L24
U 1 1 5C1A13CC
P 12800 16900
F 0 "L24" V 12900 16950 50  0000 C CNN
F 1 "1mH" V 12750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 12800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 12800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    12800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C24
U 1 1 5C1A1416
P 12650 17050
F 0 "C24" H 12550 17150 50  0000 L CNN
F 1 "1nF" H 12550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 12688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 12650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    12650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L25
U 1 1 5C1A13CE
P 13300 16900
F 0 "L25" V 13400 16950 50  0000 C CNN
F 1 "1mH" V 13250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 13300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 13300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    13300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C25
U 1 1 5C1A1417
P 13150 17050
F 0 "C25" H 13050 17150 50  0000 L CNN
F 1 "1nF" H 13050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 13188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 13150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    13150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L26
U 1 1 5C1A13CF
P 13800 16900
F 0 "L26" V 13900 16950 50  0000 C CNN
F 1 "1mH" V 13750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 13800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 13800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    13800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C26
U 1 1 5C1A1418
P 13650 17050
F 0 "C26" H 13550 17150 50  0000 L CNN
F 1 "1nF" H 13550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 13688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 13650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    13650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L27
U 1 1 5C1A13D1
P 14300 16900
F 0 "L27" V 14400 16950 50  0000 C CNN
F 1 "1mH" V 14250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 14300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 14300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    14300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C27
U 1 1 5C1A1419
P 14150 17050
F 0 "C27" H 14050 17150 50  0000 L CNN
F 1 "1nF" H 14050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 14188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 14150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    14150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L28
U 1 1 5C1A13D2
P 14800 16900
F 0 "L28" V 14900 16950 50  0000 C CNN
F 1 "1mH" V 14750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 14800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 14800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    14800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C28
U 1 1 5C1A141A
P 14650 17050
F 0 "C28" H 14550 17150 50  0000 L CNN
F 1 "1nF" H 14550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 14688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 14650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    14650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L29
U 1 1 5C1A13D4
P 15300 16900
F 0 "L29" V 15400 16950 50  0000 C CNN
F 1 "1mH" V 15250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 15300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 15300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    15300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C29
U 1 1 5C1A141B
P 15150 17050
F 0 "C29" H 15050 17150 50  0000 L CNN
F 1 "1nF" H 15050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 15188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 15150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    15150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L30
U 1 1 5C1A13D5
P 15800 16900
F 0 "L30" V 15900 16950 50  0000 C CNN
F 1 "1mH" V 15750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 15800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 15800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    15800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C30
U 1 1 5C1A141C
P 15650 17050
F 0 "C30" H 15550 17150 50  0000 L CNN
F 1 "1nF" H 15550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 15688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 15650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    15650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L31
U 1 1 5C1A13D7
P 16300 16900
F 0 "L31" V 16400 16950 50  0000 C CNN
F 1 "1mH" V 16250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 16300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 16300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    16300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C31
U 1 1 5C1A141D
P 16150 17050
F 0 "C31" H 16050 17150 50  0000 L CNN
F 1 "1nF" H 16050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 16188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 16150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    16150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L32
U 1 1 5C1A13D8
P 16800 16900
F 0 "L32" V 16900 16950 50  0000 C CNN
F 1 "1mH" V 16750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 16800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 16800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    16800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C32
U 1 1 5C18BA3A
P 16650 17050
F 0 "C32" H 16550 17150 50  0000 L CNN
F 1 "1nF" H 16550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 16688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 16650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    16650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L33
U 1 1 5C1A13CB
P 17300 16900
F 0 "L33" V 17400 16950 50  0000 C CNN
F 1 "1mH" V 17250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 17300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 17300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    17300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C33
U 1 1 5C1A140F
P 17150 17050
F 0 "C33" H 17050 17150 50  0000 L CNN
F 1 "1nF" H 17050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 17188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 17150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    17150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L34
U 1 1 5C1A13D9
P 17800 16900
F 0 "L34" V 17900 16950 50  0000 C CNN
F 1 "1mH" V 17750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 17800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 17800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    17800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C34
U 1 1 5C1A13F0
P 17650 17050
F 0 "C34" H 17550 17150 50  0000 L CNN
F 1 "1nF" H 17550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 17688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 17650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    17650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L35
U 1 1 5C1A13D6
P 18300 16900
F 0 "L35" V 18400 16950 50  0000 C CNN
F 1 "1mH" V 18250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 18300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 18300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    18300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C35
U 1 1 5C1A13F1
P 18150 17050
F 0 "C35" H 18050 17150 50  0000 L CNN
F 1 "1nF" H 18050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 18188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 18150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    18150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L36
U 1 1 5C1A13D3
P 18800 16900
F 0 "L36" V 18900 16950 50  0000 C CNN
F 1 "1mH" V 18750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 18800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 18800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    18800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C36
U 1 1 5C1A13F2
P 18650 17050
F 0 "C36" H 18550 17150 50  0000 L CNN
F 1 "1nF" H 18550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 18688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 18650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    18650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L37
U 1 1 5C1A13D0
P 19300 16900
F 0 "L37" V 19400 16950 50  0000 C CNN
F 1 "1mH" V 19250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 19300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 19300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    19300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C37
U 1 1 5C1A13F4
P 19150 17050
F 0 "C37" H 19050 17150 50  0000 L CNN
F 1 "1nF" H 19050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 19188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 19150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    19150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L38
U 1 1 5C1A13CD
P 19800 16900
F 0 "L38" V 19900 16950 50  0000 C CNN
F 1 "1mH" V 19750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 19800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 19800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    19800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C38
U 1 1 5C1A13F6
P 19650 17050
F 0 "C38" H 19550 17150 50  0000 L CNN
F 1 "1nF" H 19550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 19688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 19650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    19650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L39
U 1 1 5C1A13C0
P 20300 16900
F 0 "L39" V 20400 16950 50  0000 C CNN
F 1 "1mH" V 20250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 20300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 20300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    20300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C39
U 1 1 5C1A13F8
P 20150 17050
F 0 "C39" H 20050 17150 50  0000 L CNN
F 1 "1nF" H 20050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 20188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 20150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    20150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L40
U 1 1 5C1A13C3
P 20800 16900
F 0 "L40" V 20900 16950 50  0000 C CNN
F 1 "1mH" V 20750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 20800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 20800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    20800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C40
U 1 1 5C1A13F9
P 20650 17050
F 0 "C40" H 20550 17150 50  0000 L CNN
F 1 "1nF" H 20550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 20688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 20650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    20650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L41
U 1 1 5C1A13C6
P 21300 16900
F 0 "L41" V 21400 16950 50  0000 C CNN
F 1 "1mH" V 21250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 21300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 21300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    21300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C41
U 1 1 5C1A13FB
P 21150 17050
F 0 "C41" H 21050 17150 50  0000 L CNN
F 1 "1nF" H 21050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 21188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 21150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    21150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L42
U 1 1 5C1A13C9
P 21800 16900
F 0 "L42" V 21900 16950 50  0000 C CNN
F 1 "1mH" V 21750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 21800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 21800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    21800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C42
U 1 1 5C1A13FD
P 21650 17050
F 0 "C42" H 21550 17150 50  0000 L CNN
F 1 "1nF" H 21550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 21688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 21650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    21650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L43
U 1 1 5C18B92F
P 22300 16900
F 0 "L43" V 22400 16950 50  0000 C CNN
F 1 "1mH" V 22250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 22300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 22300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    22300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C43
U 1 1 5C1A13FC
P 22150 17050
F 0 "C43" H 22050 17150 50  0000 L CNN
F 1 "1nF" H 22050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 22188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 22150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    22150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L44
U 1 1 5C1A13E9
P 22800 16900
F 0 "L44" V 22900 16950 50  0000 C CNN
F 1 "1mH" V 22750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 22800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 22800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    22800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C44
U 1 1 5C1A13FA
P 22650 17050
F 0 "C44" H 22550 17150 50  0000 L CNN
F 1 "1nF" H 22550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 22688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 22650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    22650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L45
U 1 1 5C1A13E5
P 23300 16900
F 0 "L45" V 23400 16950 50  0000 C CNN
F 1 "1mH" V 23250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 23300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 23300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    23300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C45
U 1 1 5C1A13F7
P 23150 17050
F 0 "C45" H 23050 17150 50  0000 L CNN
F 1 "1nF" H 23050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 23188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 23150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    23150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L46
U 1 1 5C1A13E2
P 23800 16900
F 0 "L46" V 23900 16950 50  0000 C CNN
F 1 "1mH" V 23750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 23800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 23800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    23800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C46
U 1 1 5C1A13F5
P 23650 17050
F 0 "C46" H 23550 17150 50  0000 L CNN
F 1 "1nF" H 23550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 23688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 23650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    23650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L47
U 1 1 5C1A13DD
P 24300 16900
F 0 "L47" V 24400 16950 50  0000 C CNN
F 1 "1mH" V 24250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 24300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 24300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    24300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C47
U 1 1 5C1A13F3
P 24150 17050
F 0 "C47" H 24050 17150 50  0000 L CNN
F 1 "1nF" H 24050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 24188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 24150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    24150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C48
U 1 1 5C1A13EF
P 24650 17050
F 0 "C48" H 24550 17150 50  0000 L CNN
F 1 "1nF" H 24550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 24688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 24650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    24650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L49
U 1 1 5C1A13E7
P 25300 16900
F 0 "L49" V 25400 16950 50  0000 C CNN
F 1 "1mH" V 25250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 25300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 25300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    25300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C49
U 1 1 5C1A13EE
P 25150 17050
F 0 "C49" H 25050 17150 50  0000 L CNN
F 1 "1nF" H 25050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 25188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 25150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    25150 17050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 17200 1650 17200
Wire Wire Line
	1650 17200 2150 17200
Connection ~ 2150 17200
Wire Wire Line
	2150 17200 2650 17200
Connection ~ 2650 17200
Wire Wire Line
	2650 17200 3150 17200
Connection ~ 3150 17200
Wire Wire Line
	3150 17200 3650 17200
Connection ~ 3650 17200
Wire Wire Line
	3650 17200 4150 17200
Connection ~ 4150 17200
Wire Wire Line
	4150 17200 4650 17200
Connection ~ 4650 17200
Wire Wire Line
	4650 17200 5150 17200
Connection ~ 5150 17200
Wire Wire Line
	5150 17200 5650 17200
Connection ~ 5650 17200
Wire Wire Line
	5650 17200 6150 17200
Connection ~ 6150 17200
Wire Wire Line
	6150 17200 6650 17200
Connection ~ 6650 17200
Wire Wire Line
	6650 17200 7150 17200
Connection ~ 7150 17200
Wire Wire Line
	7150 17200 7650 17200
Connection ~ 7650 17200
Wire Wire Line
	7650 17200 8150 17200
Connection ~ 8150 17200
Wire Wire Line
	8150 17200 8650 17200
Wire Wire Line
	9150 17200 9650 17200
Connection ~ 9650 17200
Wire Wire Line
	9650 17200 10150 17200
Connection ~ 10150 17200
Wire Wire Line
	10150 17200 10650 17200
Connection ~ 10650 17200
Wire Wire Line
	10650 17200 11150 17200
Connection ~ 11150 17200
Wire Wire Line
	11150 17200 11650 17200
Connection ~ 11650 17200
Wire Wire Line
	11650 17200 12150 17200
Connection ~ 12150 17200
Wire Wire Line
	12150 17200 12650 17200
Connection ~ 12650 17200
Wire Wire Line
	12650 17200 13150 17200
Connection ~ 13150 17200
Wire Wire Line
	13150 17200 13650 17200
Connection ~ 13650 17200
Wire Wire Line
	13650 17200 14150 17200
Connection ~ 14150 17200
Wire Wire Line
	14150 17200 14650 17200
Connection ~ 14650 17200
Wire Wire Line
	14650 17200 15150 17200
Connection ~ 15150 17200
Wire Wire Line
	15150 17200 15650 17200
Connection ~ 15650 17200
Wire Wire Line
	15650 17200 16150 17200
Connection ~ 16150 17200
Wire Wire Line
	16150 17200 16650 17200
Wire Wire Line
	17150 17200 17650 17200
Connection ~ 17650 17200
Wire Wire Line
	17650 17200 18150 17200
Connection ~ 18150 17200
Wire Wire Line
	18150 17200 18650 17200
Connection ~ 18650 17200
Wire Wire Line
	18650 17200 19150 17200
Connection ~ 19150 17200
Wire Wire Line
	19150 17200 19650 17200
Connection ~ 19650 17200
Wire Wire Line
	19650 17200 20150 17200
Connection ~ 20150 17200
Wire Wire Line
	20150 17200 20650 17200
Connection ~ 20650 17200
Wire Wire Line
	20650 17200 21150 17200
Connection ~ 21150 17200
Wire Wire Line
	21150 17200 21650 17200
Connection ~ 21650 17200
Wire Wire Line
	21650 17200 22150 17200
Connection ~ 22150 17200
Wire Wire Line
	22150 17200 22650 17200
Connection ~ 22650 17200
Wire Wire Line
	22650 17200 23150 17200
Connection ~ 23150 17200
Wire Wire Line
	23150 17200 23650 17200
Connection ~ 23650 17200
Wire Wire Line
	23650 17200 24150 17200
Connection ~ 24150 17200
Wire Wire Line
	24150 17200 24650 17200
Connection ~ 24650 17200
Wire Wire Line
	24650 17200 25150 17200
Connection ~ 1150 17200
Connection ~ 17150 17200
Connection ~ 9150 17200
Wire Wire Line
	1450 16900 1650 16900
Wire Wire Line
	1950 16900 2150 16900
Connection ~ 2150 16900
Wire Wire Line
	2450 16900 2650 16900
Wire Wire Line
	2950 16900 3150 16900
Wire Wire Line
	3450 16900 3650 16900
Wire Wire Line
	3950 16900 4150 16900
Wire Wire Line
	4450 16900 4650 16900
Wire Wire Line
	4950 16900 5150 16900
Wire Wire Line
	5450 16900 5650 16900
Wire Wire Line
	5950 16900 6150 16900
Wire Wire Line
	6450 16900 6650 16900
Wire Wire Line
	6950 16900 7150 16900
Wire Wire Line
	7450 16900 7650 16900
Wire Wire Line
	7950 16900 8150 16900
Wire Wire Line
	8450 16900 8650 16900
Connection ~ 2650 16900
Connection ~ 3150 16900
Connection ~ 3650 16900
Connection ~ 4150 16900
Connection ~ 4650 16900
Connection ~ 5150 16900
Connection ~ 5650 16900
Connection ~ 6150 16900
Connection ~ 6650 16900
Connection ~ 7150 16900
Connection ~ 7650 16900
Connection ~ 8150 16900
Connection ~ 8650 16900
Wire Wire Line
	9450 16900 9650 16900
Wire Wire Line
	9950 16900 10150 16900
Wire Wire Line
	10450 16900 10650 16900
Wire Wire Line
	10950 16900 11150 16900
Wire Wire Line
	11450 16900 11650 16900
Wire Wire Line
	11950 16900 12150 16900
Wire Wire Line
	12450 16900 12650 16900
Wire Wire Line
	12950 16900 13150 16900
Wire Wire Line
	13450 16900 13650 16900
Wire Wire Line
	13950 16900 14150 16900
Wire Wire Line
	14450 16900 14650 16900
Wire Wire Line
	14950 16900 15150 16900
Wire Wire Line
	15450 16900 15650 16900
Wire Wire Line
	15950 16900 16150 16900
Wire Wire Line
	16450 16900 16650 16900
Connection ~ 9650 16900
Connection ~ 10150 16900
Connection ~ 10650 16900
Connection ~ 11150 16900
Connection ~ 11650 16900
Connection ~ 12150 16900
Connection ~ 12650 16900
Connection ~ 13150 16900
Connection ~ 13650 16900
Connection ~ 14150 16900
Connection ~ 14650 16900
Connection ~ 15150 16900
Connection ~ 15650 16900
Connection ~ 16150 16900
Connection ~ 16650 16900
Wire Wire Line
	17450 16900 17650 16900
Wire Wire Line
	17950 16900 18150 16900
Wire Wire Line
	18450 16900 18650 16900
Wire Wire Line
	18950 16900 19150 16900
Wire Wire Line
	19450 16900 19650 16900
Wire Wire Line
	19950 16900 20150 16900
Wire Wire Line
	20450 16900 20650 16900
Wire Wire Line
	20950 16900 21150 16900
Wire Wire Line
	21450 16900 21650 16900
Wire Wire Line
	21950 16900 22150 16900
Wire Wire Line
	22450 16900 22650 16900
Wire Wire Line
	22950 16900 23150 16900
Wire Wire Line
	23450 16900 23650 16900
Wire Wire Line
	23950 16900 24150 16900
Wire Wire Line
	24450 16900 24650 16900
Wire Wire Line
	24950 16900 25150 16900
Connection ~ 25150 16900
Connection ~ 17650 16900
Connection ~ 18150 16900
Connection ~ 18650 16900
Connection ~ 19150 16900
Connection ~ 19650 16900
Connection ~ 20150 16900
Connection ~ 20650 16900
Connection ~ 21150 16900
Connection ~ 21650 16900
Connection ~ 22150 16900
Connection ~ 22650 16900
Connection ~ 23150 16900
Connection ~ 23650 16900
Connection ~ 24150 16900
Connection ~ 24650 16900
$Comp
L Device:L L48
U 1 1 5C1A13DE
P 24800 16900
F 0 "L48" V 24900 16950 50  0000 C CNN
F 1 "1mH" V 24750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 24800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 24800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    24800 16900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 16900 17150 16900
Connection ~ 17150 16900
Wire Wire Line
	16650 17200 17150 17200
Connection ~ 16650 17200
Wire Wire Line
	8950 16900 9150 16900
Connection ~ 9150 16900
Wire Wire Line
	8650 17200 9150 17200
Connection ~ 8650 17200
Wire Wire Line
	1150 16900 850  16900
Connection ~ 1150 16900
Wire Wire Line
	25150 17200 25650 17200
Connection ~ 25150 17200
Wire Wire Line
	25450 16900 25650 16900
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5C1EFEC8
P 32350 16900
F 0 "J2" H 32350 17000 50  0000 C CNN
F 1 "Coaxial_Output" V 32450 16800 50  0000 C CNN
F 2 "Connector_Coaxial:BNC_Amphenol_B6252HB-NPP3G-50_Horizontal" H 32350 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/612848.pdf" H 32350 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/amphenol/b6252h7-npp3g-50/embase-isole-coudee-bnc-fem-sur/dp/1076289" H 32350 16900 50  0001 C CNN "Farnell"
F 5 "1,50" H 32350 16900 50  0001 C CNN "Prix Unit"
F 6 "B6252H7-NPP3G-50" H 32350 16900 50  0001 C CNN "Ref Fabricant"
	1    32350 16900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C50
U 1 1 5C212AD0
P 25650 17050
F 0 "C50" H 25550 17150 50  0000 L CNN
F 1 "1nF" H 25550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 25688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 25650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    25650 17050
	1    0    0    -1  
$EndComp
Connection ~ 25650 16900
$Comp
L Device:L L50
U 1 1 5C212BC6
P 25800 16900
F 0 "L50" V 25900 16950 50  0000 C CNN
F 1 "1mH" V 25750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 25800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 25800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    25800 16900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	26150 16900 25950 16900
Wire Wire Line
	25650 17200 26150 17200
Connection ~ 25650 17200
$Comp
L Device:L L51
U 1 1 5C6DC83C
P 26300 16900
F 0 "L51" V 26400 16950 50  0000 C CNN
F 1 "1mH" V 26250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 26300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 26300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    26300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C51
U 1 1 5C6DC842
P 26150 17050
F 0 "C51" H 26050 17150 50  0000 L CNN
F 1 "1nF" H 26050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 26188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 26150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    26150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L52
U 1 1 5C6DC848
P 26800 16900
F 0 "L52" V 26900 16950 50  0000 C CNN
F 1 "1mH" V 26750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 26800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 26800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    26800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C52
U 1 1 5C6DC84E
P 26650 17050
F 0 "C52" H 26550 17150 50  0000 L CNN
F 1 "1nF" H 26550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 26688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 26650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    26650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L53
U 1 1 5C6DC854
P 27300 16900
F 0 "L53" V 27400 16950 50  0000 C CNN
F 1 "1mH" V 27250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 27300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 27300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    27300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C53
U 1 1 5C6DC85A
P 27150 17050
F 0 "C53" H 27050 17150 50  0000 L CNN
F 1 "1nF" H 27050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 27188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 27150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    27150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L54
U 1 1 5C6DC860
P 27800 16900
F 0 "L54" V 27900 16950 50  0000 C CNN
F 1 "1mH" V 27750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 27800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 27800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    27800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C54
U 1 1 5C6DC866
P 27650 17050
F 0 "C54" H 27550 17150 50  0000 L CNN
F 1 "1nF" H 27550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 27688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 27650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    27650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L55
U 1 1 5C6DC86C
P 28300 16900
F 0 "L55" V 28400 16950 50  0000 C CNN
F 1 "1mH" V 28250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 28300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 28300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    28300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C55
U 1 1 5C6DC872
P 28150 17050
F 0 "C55" H 28050 17150 50  0000 L CNN
F 1 "1nF" H 28050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 28188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 28150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    28150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L56
U 1 1 5C6DC878
P 28800 16900
F 0 "L56" V 28900 16950 50  0000 C CNN
F 1 "1mH" V 28750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 28800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 28800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    28800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C56
U 1 1 5C6DC87E
P 28650 17050
F 0 "C56" H 28550 17150 50  0000 L CNN
F 1 "1nF" H 28550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 28688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 28650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    28650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L57
U 1 1 5C6DC884
P 29300 16900
F 0 "L57" V 29400 16950 50  0000 C CNN
F 1 "1mH" V 29250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 29300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 29300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    29300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C57
U 1 1 5C6DC88A
P 29150 17050
F 0 "C57" H 29050 17150 50  0000 L CNN
F 1 "1nF" H 29050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 29188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 29150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    29150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L58
U 1 1 5C6DC890
P 29800 16900
F 0 "L58" V 29900 16950 50  0000 C CNN
F 1 "1mH" V 29750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 29800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 29800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    29800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C58
U 1 1 5C6DC896
P 29650 17050
F 0 "C58" H 29550 17150 50  0000 L CNN
F 1 "1nF" H 29550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 29688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 29650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    29650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L59
U 1 1 5C6DC89C
P 30300 16900
F 0 "L59" V 30400 16950 50  0000 C CNN
F 1 "1mH" V 30250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 30300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 30300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    30300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C59
U 1 1 5C6DC8A2
P 30150 17050
F 0 "C59" H 30050 17150 50  0000 L CNN
F 1 "1nF" H 30050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 30188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 30150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    30150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L60
U 1 1 5C6DC8A8
P 30800 16900
F 0 "L60" V 30900 16950 50  0000 C CNN
F 1 "1mH" V 30750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 30800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 30800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    30800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C60
U 1 1 5C6DC8AE
P 30650 17050
F 0 "C60" H 30550 17150 50  0000 L CNN
F 1 "1nF" H 30550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 30688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 30650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    30650 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L61
U 1 1 5C6DC8B4
P 31300 16900
F 0 "L61" V 31400 16950 50  0000 C CNN
F 1 "1mH" V 31250 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 31300 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 31300 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    31300 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C61
U 1 1 5C6DC8BA
P 31150 17050
F 0 "C61" H 31050 17150 50  0000 L CNN
F 1 "1nF" H 31050 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 31188 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 31150 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    31150 17050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L62
U 1 1 5C6DC8C0
P 31800 16900
F 0 "L62" V 31900 16950 50  0000 C CNN
F 1 "1mH" V 31750 16900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 31800 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2724102.pdf" H 31800 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/bourns/srr7045-102m/inductance-puissance-1mh-0-25a/dp/2859214" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,327" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "SRR7045-102M" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    31800 16900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C62
U 1 1 5C6DC8C6
P 31650 17050
F 0 "C62" H 31550 17150 50  0000 L CNN
F 1 "1nF" H 31550 16950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 31688 16900 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28548/vjw1bcbascomseries.pdf" H 31650 17050 50  0001 C CNN
F 4 "https://fr.farnell.com/vishay/vj1206a102jxacw1bc/cond-1000pf-50v-5-c0g-np0-1206/dp/2896600" H -100 -250 50  0001 C CNN "Farnell"
F 5 "0,0659" H -100 -250 50  0001 C CNN "Prix Unit"
F 6 "VJ1206A102JXACW1BC" H -100 -250 50  0001 C CNN "Ref Fabricant"
	1    31650 17050
	1    0    0    -1  
$EndComp
Wire Wire Line
	26150 17200 26650 17200
Connection ~ 26650 17200
Wire Wire Line
	26650 17200 27150 17200
Connection ~ 27150 17200
Wire Wire Line
	27150 17200 27650 17200
Connection ~ 27650 17200
Wire Wire Line
	27650 17200 28150 17200
Connection ~ 28150 17200
Wire Wire Line
	28150 17200 28650 17200
Connection ~ 28650 17200
Wire Wire Line
	28650 17200 29150 17200
Connection ~ 29150 17200
Wire Wire Line
	29150 17200 29650 17200
Connection ~ 29650 17200
Wire Wire Line
	29650 17200 30150 17200
Connection ~ 30150 17200
Wire Wire Line
	30150 17200 30650 17200
Connection ~ 30650 17200
Wire Wire Line
	30650 17200 31150 17200
Connection ~ 31150 17200
Wire Wire Line
	31150 17200 31650 17200
Connection ~ 31650 17200
Wire Wire Line
	26450 16900 26650 16900
Wire Wire Line
	26950 16900 27150 16900
Wire Wire Line
	27450 16900 27650 16900
Wire Wire Line
	27950 16900 28150 16900
Wire Wire Line
	28450 16900 28650 16900
Wire Wire Line
	28950 16900 29150 16900
Wire Wire Line
	29450 16900 29650 16900
Wire Wire Line
	29950 16900 30150 16900
Wire Wire Line
	30450 16900 30650 16900
Wire Wire Line
	30950 16900 31150 16900
Wire Wire Line
	31450 16900 31650 16900
Connection ~ 26650 16900
Connection ~ 27150 16900
Connection ~ 27650 16900
Connection ~ 28150 16900
Connection ~ 28650 16900
Connection ~ 29150 16900
Connection ~ 29650 16900
Connection ~ 30150 16900
Connection ~ 30650 16900
Connection ~ 31150 16900
Connection ~ 31650 16900
Connection ~ 26150 16900
Connection ~ 26150 17200
Text Label 1150 16900 1    50   ~ 0
mes1
Text Label 2150 16900 1    50   ~ 0
mes2
Text Label 3150 16900 1    50   ~ 0
mes3
Text Label 4150 16900 1    50   ~ 0
mes4
Text Label 5150 16900 1    50   ~ 0
mes5
Text Label 6150 16900 1    50   ~ 0
mes6
Text Label 7150 16900 1    50   ~ 0
mes7
Text Label 8150 16900 1    50   ~ 0
mes8
Text Label 9150 16900 1    50   ~ 0
mes9
Text Label 10150 16900 1    50   ~ 0
mes10
Text Label 11150 16900 1    50   ~ 0
mes11
Text Label 12150 16900 1    50   ~ 0
mes12
Text Label 13150 16900 1    50   ~ 0
mes13
Text Label 14150 16900 1    50   ~ 0
mes14
Text Label 15150 16900 1    50   ~ 0
mes15
Text Label 16150 16900 1    50   ~ 0
mes16
Text Label 17150 16900 1    50   ~ 0
mes17
Text Label 18150 16900 1    50   ~ 0
mes18
Text Label 19150 16900 1    50   ~ 0
mes19
Text Label 20150 16900 1    50   ~ 0
mes20
Text Label 21150 16900 1    50   ~ 0
mes21
Text Label 22150 16900 1    50   ~ 0
mes22
Text Label 23150 16900 1    50   ~ 0
mes23
Text Label 24150 16900 1    50   ~ 0
mes24
Text Label 25150 16900 1    50   ~ 0
mes25
Text Label 26150 16900 1    50   ~ 0
mes26
Text Label 27150 16900 1    50   ~ 0
mes27
Text Label 28150 16900 1    50   ~ 0
mes28
Text Label 29150 16900 1    50   ~ 0
mes29
Text Label 30150 16900 1    50   ~ 0
mes30
Text Label 31150 16900 1    50   ~ 0
mes31
Text Label 32150 16900 1    50   ~ 0
mes32
Connection ~ 1650 16900
Connection ~ 1650 17200
$Comp
L power:GND #PWR01
U 1 1 5CC4A476
P 1150 17200
F 0 "#PWR01" H 1150 16950 50  0001 C CNN
F 1 "GND" H 1155 17027 50  0000 C CNN
F 2 "" H 1150 17200 50  0001 C CNN
F 3 "" H 1150 17200 50  0001 C CNN
	1    1150 17200
	1    0    0    -1  
$EndComp
Text Notes 8700 17850 0    50   ~ 0
Valeurs :\nC = 1nF\nC/2 = 500pF\nL = 1mH
Wire Wire Line
	31950 16900 32150 16900
Wire Wire Line
	4150 16900 4150 16550
Text Label 4150 16550 1    50   ~ 0
TestPoint1
Wire Wire Line
	9150 16900 9150 16550
Text Label 9150 16550 1    50   ~ 0
TestPoint2
Wire Wire Line
	14150 16900 14150 16550
Text Label 14150 16550 1    50   ~ 0
TestPoint3
Wire Wire Line
	19150 16900 19150 16550
Text Label 19150 16550 1    50   ~ 0
TestPoint4
Wire Wire Line
	24150 16900 24150 16550
Text Label 24150 16550 1    50   ~ 0
TestPoint5
Wire Wire Line
	29150 16900 29150 16550
Text Label 29150 16550 1    50   ~ 0
TestPoint6
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DD4A93B
P 12000 3100
F 0 "#FLG0101" H 12000 3175 50  0001 C CNN
F 1 "PWR_FLAG" H 12000 3273 50  0000 C CNN
F 2 "" H 12000 3100 50  0001 C CNN
F 3 "~" H 12000 3100 50  0001 C CNN
	1    12000 3100
	1    0    0    -1  
$EndComp
Connection ~ 12000 3100
Wire Wire Line
	16650 16900 16650 16600
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO_PHELMA1
U 1 1 5DDADFEC
P 30000 4000
F 0 "LOGO_PHELMA1" H 30000 4500 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 30000 3600 50  0001 C CNN
F 2 "logos:logo_phelma_32x21mm" H 30000 4000 50  0001 C CNN
F 3 "~" H 30000 4000 50  0001 C CNN
F 4 "0" H 30000 4000 50  0001 C CNN "Prix Unit"
	1    30000 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5C1CFC17
P 650 16900
F 0 "J1" H 650 17000 50  0000 C CNN
F 1 "Coaxial_Input" V 750 16750 50  0000 C CNN
F 2 "Connector_Coaxial:BNC_Amphenol_B6252HB-NPP3G-50_Horizontal" H 650 16900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/612848.pdf" H 650 16900 50  0001 C CNN
F 4 "https://fr.farnell.com/amphenol/b6252h7-npp3g-50/embase-isole-coudee-bnc-fem-sur/dp/1076289" H 650 16900 50  0001 C CNN "Farnell"
F 5 "1,50" H 650 16900 50  0001 C CNN "Prix Unit"
F 6 "B6252H7-NPP3G-50" H 650 16900 50  0001 C CNN "Ref Fabricant"
	1    650  16900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	650  17100 650  17200
Wire Wire Line
	650  17200 1150 17200
Wire Wire Line
	32350 17200 32350 17100
Wire Wire Line
	31650 17200 32350 17200
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO1
U 1 1 5DBF2DF0
P 30000 5000
F 0 "LOGO1" H 30000 5500 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 30000 4600 50  0001 C CNN
F 2 "Symbol:OSHW-Logo_7.5x8mm_SilkScreen" H 30000 5000 50  0001 C CNN
F 3 "~" H 30000 5000 50  0001 C CNN
F 4 "0" H 30000 5000 50  0001 C CNN "Prix Unit"
	1    30000 5000
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO2
U 1 1 5DBF35D6
P 31000 5000
F 0 "LOGO2" H 31000 5500 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 31000 4600 50  0001 C CNN
F 2 "Symbol:KiCad-Logo2_5mm_SilkScreen" H 31000 5000 50  0001 C CNN
F 3 "~" H 31000 5000 50  0001 C CNN
F 4 "0" H 31000 5000 50  0001 C CNN "Prix Unit"
	1    31000 5000
	1    0    0    -1  
$EndComp
Text Label 7600 3400 2    50   ~ 0
~S~|H
Text Label 7600 5000 2    50   ~ 0
~S~|H
Text Label 7600 6600 2    50   ~ 0
~S~|H
Text Label 7600 8200 2    50   ~ 0
~S~|H
Text Label 12000 4000 0    50   ~ 0
~S~|H
Text Label 7600 14600 2    50   ~ 0
~S~|H
Text Label 7600 13000 2    50   ~ 0
~S~|H
Text Label 7600 11400 2    50   ~ 0
~S~|H
Text Label 7600 9800 2    50   ~ 0
~S~|H
Text Label 11500 4100 2    50   ~ 0
~S~|H
$EndSCHEMATC
