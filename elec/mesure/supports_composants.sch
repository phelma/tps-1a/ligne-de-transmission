EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC4051 U?
U 1 1 5DAC1BE0
P 4550 2150
AR Path="/5C1AA865/5DAC1BE0" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DAC1BE0" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DAC1BE0" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1BE0" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1BE0" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1BE0" Ref="U?"  Part="1" 
F 0 "U?" V 4650 2600 50  0000 L CNN
F 1 "74HC4051" V 4750 2600 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4550 1750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4550 1750 50  0001 C CNN
F 4 "https://fr.farnell.com/nexperia/74hc4051d/ci-cmos-multiplex-demultiplex/dp/1201327" H 1500 -900 50  0001 C CNN "Farnell"
F 5 "0,237" H 1500 -900 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1500 -900 50  0001 C CNN "Ref Fabricant"
	1    4550 2150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DAC1BE6
P 3950 2150
AR Path="/5C1A74E1/5C3BCAE0/5DAC1BE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1BE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1BE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1BE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1BE6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 1900 50  0001 C CNN
F 1 "GND" V 3950 2050 50  0000 R CNN
F 2 "" H 3950 2150 50  0001 C CNN
F 3 "" H 3950 2150 50  0001 C CNN
	1    3950 2150
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DAC1BEC
P 5050 2150
AR Path="/5C1A74E1/5C3BCAE0/5DAC1BEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1BEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1BEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1BEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1BEC" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5050 2000 50  0001 C CNN
F 1 "+5V" V 5050 2250 50  0000 L CNN
F 2 "" H 5050 2150 50  0001 C CNN
F 3 "" H 5050 2150 50  0001 C CNN
	1    5050 2150
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DAC1BF2
P 3950 2250
AR Path="/5C1A74E1/5C3BCAE0/5DAC1BF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1BF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1BF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1BF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1BF2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 2350 50  0001 C CNN
F 1 "-5V" V 3950 2350 50  0000 L CNN
F 2 "" H 3950 2250 50  0001 C CNN
F 3 "" H 3950 2250 50  0001 C CNN
	1    3950 2250
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 5DAC1BFB
P 4550 3400
AR Path="/5C1AA865/5DAC1BFB" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DAC1BFB" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DAC1BFB" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1BFB" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1BFB" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1BFB" Ref="U?"  Part="1" 
F 0 "U?" V 4650 3850 50  0000 L CNN
F 1 "74HC4051" V 4750 3850 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4550 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4550 3000 50  0001 C CNN
F 4 "https://fr.farnell.com/nexperia/74hc4051d/ci-cmos-multiplex-demultiplex/dp/1201327" H 1500 -900 50  0001 C CNN "Farnell"
F 5 "0,237" H 1500 -900 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1500 -900 50  0001 C CNN "Ref Fabricant"
	1    4550 3400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DAC1C01
P 3950 3400
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C01" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C01" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C01" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C01" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C01" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 3150 50  0001 C CNN
F 1 "GND" V 3950 3300 50  0000 R CNN
F 2 "" H 3950 3400 50  0001 C CNN
F 3 "" H 3950 3400 50  0001 C CNN
	1    3950 3400
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DAC1C07
P 5050 3400
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C07" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C07" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C07" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C07" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C07" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5050 3250 50  0001 C CNN
F 1 "+5V" V 5050 3500 50  0000 L CNN
F 2 "" H 5050 3400 50  0001 C CNN
F 3 "" H 5050 3400 50  0001 C CNN
	1    5050 3400
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DAC1C0D
P 3950 3500
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C0D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C0D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 3600 50  0001 C CNN
F 1 "-5V" V 3950 3600 50  0000 L CNN
F 2 "" H 3950 3500 50  0001 C CNN
F 3 "" H 3950 3500 50  0001 C CNN
	1    3950 3500
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 5DAC1C16
P 4550 4700
AR Path="/5C1AA865/5DAC1C16" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C16" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C16" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C16" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C16" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C16" Ref="U?"  Part="1" 
F 0 "U?" V 4650 5150 50  0000 L CNN
F 1 "74HC4051" V 4750 5150 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4550 4300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4550 4300 50  0001 C CNN
F 4 "https://fr.farnell.com/nexperia/74hc4051d/ci-cmos-multiplex-demultiplex/dp/1201327" H 1500 -900 50  0001 C CNN "Farnell"
F 5 "0,237" H 1500 -900 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1500 -900 50  0001 C CNN "Ref Fabricant"
	1    4550 4700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DAC1C1C
P 3950 4700
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C1C" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C1C" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C1C" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C1C" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C1C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 4450 50  0001 C CNN
F 1 "GND" V 3950 4600 50  0000 R CNN
F 2 "" H 3950 4700 50  0001 C CNN
F 3 "" H 3950 4700 50  0001 C CNN
	1    3950 4700
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DAC1C22
P 5050 4700
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C22" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C22" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C22" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C22" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C22" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5050 4550 50  0001 C CNN
F 1 "+5V" V 5050 4800 50  0000 L CNN
F 2 "" H 5050 4700 50  0001 C CNN
F 3 "" H 5050 4700 50  0001 C CNN
	1    5050 4700
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DAC1C28
P 3950 4800
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C28" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C28" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C28" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C28" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C28" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 4900 50  0001 C CNN
F 1 "-5V" V 3950 4900 50  0000 L CNN
F 2 "" H 3950 4800 50  0001 C CNN
F 3 "" H 3950 4800 50  0001 C CNN
	1    3950 4800
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 5DAC1C31
P 4550 6000
AR Path="/5C1AA865/5DAC1C31" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C31" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C31" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C31" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C31" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C31" Ref="U?"  Part="1" 
F 0 "U?" V 4650 6450 50  0000 L CNN
F 1 "74HC4051" V 4750 6450 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4550 5600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4550 5600 50  0001 C CNN
F 4 "https://fr.farnell.com/nexperia/74hc4051d/ci-cmos-multiplex-demultiplex/dp/1201327" H 1500 -900 50  0001 C CNN "Farnell"
F 5 "0,237" H 1500 -900 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1500 -900 50  0001 C CNN "Ref Fabricant"
	1    4550 6000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DAC1C37
P 3950 6000
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C37" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C37" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C37" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C37" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C37" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 5750 50  0001 C CNN
F 1 "GND" V 3950 5900 50  0000 R CNN
F 2 "" H 3950 6000 50  0001 C CNN
F 3 "" H 3950 6000 50  0001 C CNN
	1    3950 6000
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DAC1C3D
P 5050 6000
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C3D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C3D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C3D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C3D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C3D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5050 5850 50  0001 C CNN
F 1 "+5V" V 5050 6100 50  0000 L CNN
F 2 "" H 5050 6000 50  0001 C CNN
F 3 "" H 5050 6000 50  0001 C CNN
	1    5050 6000
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DAC1C43
P 3950 6100
AR Path="/5C1A74E1/5C3BCAE0/5DAC1C43" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DAC1C43" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DAC1C43" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DAC1C43" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C43" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 6200 50  0001 C CNN
F 1 "-5V" V 3950 6200 50  0000 L CNN
F 2 "" H 3950 6100 50  0001 C CNN
F 3 "" H 3950 6100 50  0001 C CNN
	1    3950 6100
	0    -1   -1   0   
$EndComp
Text Label 4850 2550 3    50   ~ 0
hold_out_1
Text Label 4750 2550 3    50   ~ 0
hold_out_2
Text Label 4650 2550 3    50   ~ 0
hold_out_3
Text Label 4550 2550 3    50   ~ 0
hold_out_4
Text Label 4450 2550 3    50   ~ 0
hold_out_5
Text Label 4350 2550 3    50   ~ 0
hold_out_6
Text Label 4250 2550 3    50   ~ 0
hold_out_7
Text Label 4150 2550 3    50   ~ 0
hold_out_8
Text Label 4850 3800 3    50   ~ 0
hold_out_9
Text Label 4750 3800 3    50   ~ 0
hold_out_10
Text Label 4650 3800 3    50   ~ 0
hold_out_11
Text Label 4550 3800 3    50   ~ 0
hold_out_12
Text Label 4450 3800 3    50   ~ 0
hold_out_13
Text Label 4350 3800 3    50   ~ 0
hold_out_14
Text Label 4250 3800 3    50   ~ 0
hold_out_15
Text Label 4150 3800 3    50   ~ 0
hold_out_16
Text Label 4850 5100 3    50   ~ 0
hold_out_17
Text Label 4750 5100 3    50   ~ 0
hold_out_18
Text Label 4650 5100 3    50   ~ 0
hold_out_19
Text Label 4550 5100 3    50   ~ 0
hold_out_20
Text Label 4450 5100 3    50   ~ 0
hold_out_21
Text Label 4350 5100 3    50   ~ 0
hold_out_22
Text Label 4250 5100 3    50   ~ 0
hold_out_23
Text Label 4150 5100 3    50   ~ 0
hold_out_24
Text Label 4850 6400 3    50   ~ 0
hold_out_25
Text Label 4750 6400 3    50   ~ 0
hold_out_26
Text Label 4650 6400 3    50   ~ 0
hold_out_27
Text Label 4550 6400 3    50   ~ 0
hold_out_28
Text Label 4450 6400 3    50   ~ 0
hold_out_29
Text Label 4350 6400 3    50   ~ 0
hold_out_30
Text Label 4250 6400 3    50   ~ 0
hold_out_31
Text Label 4150 6400 3    50   ~ 0
hold_out_32
Text Label 4650 1850 0    50   ~ 0
S0
Text Label 4550 1850 0    50   ~ 0
S1
Text Label 4450 1850 0    50   ~ 0
S2
Text Label 4650 3100 0    50   ~ 0
S0
Text Label 4550 3100 0    50   ~ 0
S1
Text Label 4450 3100 0    50   ~ 0
S2
Text Label 4650 4400 0    50   ~ 0
S0
Text Label 4550 4400 0    50   ~ 0
S1
Text Label 4450 4400 0    50   ~ 0
S2
Text Label 4650 5700 0    50   ~ 0
S0
Text Label 4550 5700 0    50   ~ 0
S1
Text Label 4450 5700 0    50   ~ 0
S2
Text Label 4850 5700 0    50   ~ 0
Out
Text Label 4850 4400 0    50   ~ 0
Out
Text Label 4850 3100 0    50   ~ 0
Out
Text Label 4850 1850 0    50   ~ 0
Out
Text Label 4250 1850 0    50   ~ 0
S3
Text Label 4250 3100 0    50   ~ 0
S4
Text Label 4250 4400 0    50   ~ 0
S5
Text Label 4250 5700 0    50   ~ 0
S6
Text Label 3000 3200 0    50   ~ 0
hold_out_2
Text Label 3000 3300 0    50   ~ 0
hold_out_4
Text Label 3000 3400 0    50   ~ 0
hold_out_6
Text Label 3000 3500 0    50   ~ 0
hold_out_8
Text Label 3000 3600 0    50   ~ 0
hold_out_10
Text Label 3000 3700 0    50   ~ 0
hold_out_12
Text Label 3000 3800 0    50   ~ 0
hold_out_14
Text Label 3000 3900 0    50   ~ 0
hold_out_16
Text Label 3000 4200 0    50   ~ 0
hold_out_18
Text Label 3000 4300 0    50   ~ 0
hold_out_20
Text Label 3000 4400 0    50   ~ 0
hold_out_22
Text Label 3000 4500 0    50   ~ 0
hold_out_24
Text Label 3000 4600 0    50   ~ 0
hold_out_26
Text Label 3000 4700 0    50   ~ 0
hold_out_28
Text Label 3000 4800 0    50   ~ 0
hold_out_30
Text Label 3000 4900 0    50   ~ 0
hold_out_32
$Comp
L power:GND #PWR?
U 1 1 5DAC1C9D
P 3000 3100
AR Path="/5C1AA865/5DAC1C9D" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DAC1C9D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3000 2850 50  0001 C CNN
F 1 "GND" V 3000 3000 50  0000 R CNN
F 2 "" H 3000 3100 50  0001 C CNN
F 3 "" H 3000 3100 50  0001 C CNN
	1    3000 3100
	0    -1   -1   0   
$EndComp
Text Label 3000 4100 0    50   ~ 0
Vline-
Text Label 3000 5000 0    50   ~ 0
Vline+
Text Label 3000 4000 0    50   ~ 0
CLK
$Comp
L Connector_Generic:Conn_01x20 J?
U 1 1 5DAC1CB2
P 2800 4100
AR Path="/5C1AA865/5DAC1CB2" Ref="J?"  Part="1" 
AR Path="/5C1A74E1/5DAC1CB2" Ref="J?"  Part="1" 
AR Path="/5DAB4208/5DAC1CB2" Ref="J?"  Part="1" 
F 0 "J?" H 2850 5200 50  0000 C CNN
F 1 "Conn_Ligne_out" H 2850 5100 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x20_P2.54mm_Vertical" H 2800 4100 50  0001 C CNN
F 3 "~" H 2800 4100 50  0001 C CNN
F 4 "https://fr.farnell.com/amphenol/t821140a1s100ceu/embase-vertical-2-54mm-40voies/dp/2215314" H 1600 -800 50  0001 C CNN "Farnell"
F 5 "0,597" H 1600 -800 50  0001 C CNN "Prix Unit"
F 6 "T821140A1S100CEU" H 1600 -800 50  0001 C CNN "Ref Fabricant"
	1    2800 4100
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 5DAC7D97
P 7150 3000
F 0 "J?" V 7114 2612 50  0000 R CNN
F 1 "Conn_01x06" V 7023 2612 50  0000 R CNN
F 2 "" H 7150 3000 50  0001 C CNN
F 3 "~" H 7150 3000 50  0001 C CNN
	1    7150 3000
	0    -1   -1   0   
$EndComp
Text Label 6950 3200 3    50   ~ 0
S0
$EndSCHEMATC
