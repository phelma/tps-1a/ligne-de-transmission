EESchema Schematic File Version 4
LIBS:TP_LigneDeTransmission_mesure-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC4051 U?
U 1 1 5DC75EA4
P 4500 1700
AR Path="/5C1AA865/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5DC75EA4" Ref="U?"  Part="1" 
AR Path="/5DC66300/5DC75EA4" Ref="U1"  Part="1" 
F 0 "U1" V 4600 2150 50  0000 L CNN
F 1 "74HC4051" V 4700 2150 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4500 1300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4500 1300 50  0001 C CNN
F 4 "1201327" H 1450 -1350 50  0001 C CNN "Farnell"
F 5 "0,325" H 1450 -1350 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1450 -1350 50  0001 C CNN "Ref Fabricant"
	1    4500 1700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DC75EAA
P 3900 1700
AR Path="/5C1A74E1/5C3BCAE0/5DC75EAA" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EAA" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EAA" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EAA" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EAA" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EAA" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EAA" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 3900 1450 50  0001 C CNN
F 1 "GND" V 3900 1600 50  0000 R CNN
F 2 "" H 3900 1700 50  0001 C CNN
F 3 "" H 3900 1700 50  0001 C CNN
	1    3900 1700
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DC75EB0
P 5000 1700
AR Path="/5C1A74E1/5C3BCAE0/5DC75EB0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EB0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EB0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EB0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EB0" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EB0" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EB0" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 5000 1550 50  0001 C CNN
F 1 "+5V" V 5000 1800 50  0000 L CNN
F 2 "" H 5000 1700 50  0001 C CNN
F 3 "" H 5000 1700 50  0001 C CNN
	1    5000 1700
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DC75EB6
P 3900 1800
AR Path="/5C1A74E1/5C3BCAE0/5DC75EB6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EB6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EB6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EB6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EB6" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EB6" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EB6" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 3900 1900 50  0001 C CNN
F 1 "-5V" V 3900 1900 50  0000 L CNN
F 2 "" H 3900 1800 50  0001 C CNN
F 3 "" H 3900 1800 50  0001 C CNN
	1    3900 1800
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 5DC75EBF
P 4500 3200
AR Path="/5C1AA865/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5DC75EBF" Ref="U?"  Part="1" 
AR Path="/5DC66300/5DC75EBF" Ref="U2"  Part="1" 
F 0 "U2" V 4600 3650 50  0000 L CNN
F 1 "74HC4051" V 4700 3650 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4500 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4500 2800 50  0001 C CNN
F 4 "1201327" H 1450 -1100 50  0001 C CNN "Farnell"
F 5 "0,325" H 1450 -1100 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1450 -1100 50  0001 C CNN "Ref Fabricant"
	1    4500 3200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DC75EC5
P 3900 3200
AR Path="/5C1A74E1/5C3BCAE0/5DC75EC5" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EC5" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EC5" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EC5" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EC5" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EC5" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EC5" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 3900 2950 50  0001 C CNN
F 1 "GND" V 3900 3100 50  0000 R CNN
F 2 "" H 3900 3200 50  0001 C CNN
F 3 "" H 3900 3200 50  0001 C CNN
	1    3900 3200
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DC75ECB
P 5000 3200
AR Path="/5C1A74E1/5C3BCAE0/5DC75ECB" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75ECB" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75ECB" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75ECB" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75ECB" Ref="#PWR?"  Part="1" 
AR Path="/5DC75ECB" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75ECB" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 5000 3050 50  0001 C CNN
F 1 "+5V" V 5000 3300 50  0000 L CNN
F 2 "" H 5000 3200 50  0001 C CNN
F 3 "" H 5000 3200 50  0001 C CNN
	1    5000 3200
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DC75ED1
P 3900 3300
AR Path="/5C1A74E1/5C3BCAE0/5DC75ED1" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75ED1" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75ED1" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75ED1" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75ED1" Ref="#PWR?"  Part="1" 
AR Path="/5DC75ED1" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75ED1" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 3900 3400 50  0001 C CNN
F 1 "-5V" V 3900 3400 50  0000 L CNN
F 2 "" H 3900 3300 50  0001 C CNN
F 3 "" H 3900 3300 50  0001 C CNN
	1    3900 3300
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74HC4051 U?
U 1 1 5DC75EDA
P 4500 4700
AR Path="/5C1AA865/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5DC75EDA" Ref="U?"  Part="1" 
AR Path="/5DC66300/5DC75EDA" Ref="U3"  Part="1" 
F 0 "U3" V 4600 5150 50  0000 L CNN
F 1 "74HC4051" V 4700 5150 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4500 4300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4500 4300 50  0001 C CNN
F 4 "1201327" H 1450 -900 50  0001 C CNN "Farnell"
F 5 "0,325" H 1450 -900 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1450 -900 50  0001 C CNN "Ref Fabricant"
	1    4500 4700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DC75EE0
P 3900 4700
AR Path="/5C1A74E1/5C3BCAE0/5DC75EE0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EE0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EE0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EE0" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EE0" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EE0" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EE0" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 3900 4450 50  0001 C CNN
F 1 "GND" V 3900 4600 50  0000 R CNN
F 2 "" H 3900 4700 50  0001 C CNN
F 3 "" H 3900 4700 50  0001 C CNN
	1    3900 4700
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DC75EE6
P 5000 4700
AR Path="/5C1A74E1/5C3BCAE0/5DC75EE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EE6" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EE6" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EE6" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EE6" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 5000 4550 50  0001 C CNN
F 1 "+5V" V 5000 4800 50  0000 L CNN
F 2 "" H 5000 4700 50  0001 C CNN
F 3 "" H 5000 4700 50  0001 C CNN
	1    5000 4700
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DC75EEC
P 3900 4800
AR Path="/5C1A74E1/5C3BCAE0/5DC75EEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EEC" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EEC" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EEC" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EEC" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 3900 4900 50  0001 C CNN
F 1 "-5V" V 3900 4900 50  0000 L CNN
F 2 "" H 3900 4800 50  0001 C CNN
F 3 "" H 3900 4800 50  0001 C CNN
	1    3900 4800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DC75EF2
P 3900 6200
AR Path="/5C1A74E1/5C3BCAE0/5DC75EF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EF2" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EF2" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EF2" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EF2" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 3900 5950 50  0001 C CNN
F 1 "GND" V 3900 6100 50  0000 R CNN
F 2 "" H 3900 6200 50  0001 C CNN
F 3 "" H 3900 6200 50  0001 C CNN
	1    3900 6200
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DC75EF8
P 5000 6200
AR Path="/5C1A74E1/5C3BCAE0/5DC75EF8" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EF8" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EF8" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EF8" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EF8" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EF8" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EF8" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 5000 6050 50  0001 C CNN
F 1 "+5V" V 5000 6300 50  0000 L CNN
F 2 "" H 5000 6200 50  0001 C CNN
F 3 "" H 5000 6200 50  0001 C CNN
	1    5000 6200
	0    1    1    0   
$EndComp
$Comp
L power:-5V #PWR?
U 1 1 5DC75EFE
P 3900 6300
AR Path="/5C1A74E1/5C3BCAE0/5DC75EFE" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75EFE" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75EFE" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75EFE" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75EFE" Ref="#PWR?"  Part="1" 
AR Path="/5DC75EFE" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75EFE" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 3900 6400 50  0001 C CNN
F 1 "-5V" V 3900 6400 50  0000 L CNN
F 2 "" H 3900 6300 50  0001 C CNN
F 3 "" H 3900 6300 50  0001 C CNN
	1    3900 6300
	0    -1   -1   0   
$EndComp
Text Label 4800 5900 0    50   ~ 0
Out
Text Label 4800 4400 0    50   ~ 0
Out
Text Label 4800 2900 0    50   ~ 0
Out
Text Label 2000 2000 2    50   ~ 0
Out
Text Label 2000 4600 2    50   ~ 0
hold_out_1
Text Label 2000 4700 2    50   ~ 0
hold_out_3
Text Label 2000 4800 2    50   ~ 0
hold_out_5
Text Label 2000 4900 2    50   ~ 0
hold_out_7
Text Label 2000 5000 2    50   ~ 0
hold_out_9
Text Label 2000 5100 2    50   ~ 0
hold_out_11
Text Label 2000 5200 2    50   ~ 0
hold_out_13
Text Label 2000 5300 2    50   ~ 0
hold_out_15
Text Label 2000 5600 2    50   ~ 0
hold_out_17
Text Label 2000 5700 2    50   ~ 0
hold_out_19
Text Label 2000 5800 2    50   ~ 0
hold_out_21
Text Label 2000 5900 2    50   ~ 0
hold_out_23
Text Label 2000 6000 2    50   ~ 0
hold_out_25
Text Label 2000 6100 2    50   ~ 0
hold_out_27
Text Label 2000 6200 2    50   ~ 0
hold_out_29
Text Label 2000 6300 2    50   ~ 0
hold_out_31
Text Label 2500 4600 0    50   ~ 0
hold_out_2
Text Label 2500 4700 0    50   ~ 0
hold_out_4
Text Label 2500 4800 0    50   ~ 0
hold_out_6
Text Label 2500 4900 0    50   ~ 0
hold_out_8
Text Label 2500 5000 0    50   ~ 0
hold_out_10
Text Label 2500 5100 0    50   ~ 0
hold_out_12
Text Label 2500 5200 0    50   ~ 0
hold_out_14
Text Label 2500 5300 0    50   ~ 0
hold_out_16
Text Label 2500 5600 0    50   ~ 0
hold_out_18
Text Label 2500 5700 0    50   ~ 0
hold_out_20
Text Label 2500 5800 0    50   ~ 0
hold_out_22
Text Label 2500 5900 0    50   ~ 0
hold_out_24
Text Label 2500 6000 0    50   ~ 0
hold_out_26
Text Label 2500 6100 0    50   ~ 0
hold_out_28
Text Label 2500 6200 0    50   ~ 0
hold_out_30
Text Label 2500 6300 0    50   ~ 0
hold_out_32
$Comp
L power:GND #PWR?
U 1 1 5DC75F28
P 2500 4500
AR Path="/5C1AA865/5DC75F28" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75F28" Ref="#PWR?"  Part="1" 
AR Path="/5DC75F28" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75F28" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 2500 4250 50  0001 C CNN
F 1 "GND" V 2500 4400 50  0000 R CNN
F 2 "" H 2500 4500 50  0001 C CNN
F 3 "" H 2500 4500 50  0001 C CNN
	1    2500 4500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DC75F2E
P 2000 6400
AR Path="/5C1AA865/5DC75F2E" Ref="#PWR?"  Part="1" 
AR Path="/5C1A74E1/5DC75F2E" Ref="#PWR?"  Part="1" 
AR Path="/5DC75F2E" Ref="#PWR?"  Part="1" 
AR Path="/5DC66300/5DC75F2E" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 2000 6150 50  0001 C CNN
F 1 "GND" V 2000 6300 50  0000 R CNN
F 2 "" H 2000 6400 50  0001 C CNN
F 3 "" H 2000 6400 50  0001 C CNN
	1    2000 6400
	0    1    1    0   
$EndComp
Text Label 2000 4500 2    50   ~ 0
Vline+
Text Label 2500 5500 0    50   ~ 0
Vline-
Text Label 2500 6400 0    50   ~ 0
Vline+
Text Label 2000 5400 2    50   ~ 0
Vline-
$Comp
L 74xx:74HC4051 U?
U 1 1 5DC75F3D
P 4500 6200
AR Path="/5C1AA865/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3BCAE0/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C0050/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3C280E/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5C1A74E1/5C3CEAFB/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5DC75F3D" Ref="U?"  Part="1" 
AR Path="/5DC66300/5DC75F3D" Ref="U4"  Part="1" 
F 0 "U4" V 4600 6650 50  0000 L CNN
F 1 "74HC4051" V 4700 6650 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4500 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4051.pdf" H 4500 5800 50  0001 C CNN
F 4 "1201327" H 1450 -700 50  0001 C CNN "Farnell"
F 5 "0,325" H 1450 -700 50  0001 C CNN "Prix Unit"
F 6 "74HC4051D" H 1450 -700 50  0001 C CNN "Ref Fabricant"
	1    4500 6200
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5DC75F46
P 2200 5500
AR Path="/5C1AA865/5DC75F46" Ref="J?"  Part="1" 
AR Path="/5C1A74E1/5DC75F46" Ref="J?"  Part="1" 
AR Path="/5DC75F46" Ref="J?"  Part="1" 
AR Path="/5DC66300/5DC75F46" Ref="J1"  Part="1" 
F 0 "J1" H 2250 6600 50  0000 C CNN
F 1 "Conn_Ligne_out" H 2250 6500 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Horizontal" H 2200 5500 50  0001 C CNN
F 3 "~" H 2200 5500 50  0001 C CNN
F 4 "1668400" H 1000 600 50  0001 C CNN "Farnell"
F 5 "5,45" H 1000 600 50  0001 C CNN "Prix Unit"
F 6 "SSW-120-02-G-D-RA" H 1000 600 50  0001 C CNN "Ref Fabricant"
	1    2200 5500
	1    0    0    1   
$EndComp
Text Label 4100 2100 3    50   ~ 0
hold_out_8
Text Label 4200 2100 3    50   ~ 0
hold_out_7
Text Label 4300 2100 3    50   ~ 0
hold_out_6
Text Label 4400 2100 3    50   ~ 0
hold_out_5
Text Label 4500 2100 3    50   ~ 0
hold_out_4
Text Label 4600 2100 3    50   ~ 0
hold_out_3
Text Label 4700 2100 3    50   ~ 0
hold_out_2
Text Label 4800 2100 3    50   ~ 0
hold_out_1
Text Label 4100 6600 3    50   ~ 0
hold_out_32
Text Label 4200 6600 3    50   ~ 0
hold_out_31
Text Label 4300 6600 3    50   ~ 0
hold_out_30
Text Label 4400 6600 3    50   ~ 0
hold_out_29
Text Label 4500 6600 3    50   ~ 0
hold_out_28
Text Label 4600 6600 3    50   ~ 0
hold_out_27
Text Label 4700 6600 3    50   ~ 0
hold_out_26
Text Label 4800 6600 3    50   ~ 0
hold_out_25
Text Label 4100 5100 3    50   ~ 0
hold_out_24
Text Label 4200 5100 3    50   ~ 0
hold_out_23
Text Label 4300 5100 3    50   ~ 0
hold_out_22
Text Label 4400 5100 3    50   ~ 0
hold_out_21
Text Label 4500 5100 3    50   ~ 0
hold_out_20
Text Label 4600 5100 3    50   ~ 0
hold_out_19
Text Label 4700 5100 3    50   ~ 0
hold_out_18
Text Label 4800 5100 3    50   ~ 0
hold_out_17
Text Label 4100 3600 3    50   ~ 0
hold_out_16
Text Label 4200 3600 3    50   ~ 0
hold_out_15
Text Label 4300 3600 3    50   ~ 0
hold_out_14
Text Label 4400 3600 3    50   ~ 0
hold_out_13
Text Label 4500 3600 3    50   ~ 0
hold_out_12
Text Label 4600 3600 3    50   ~ 0
hold_out_11
Text Label 4700 3600 3    50   ~ 0
hold_out_10
Text Label 4800 3600 3    50   ~ 0
hold_out_9
Text Label 2000 3000 2    50   ~ 0
Vline+
Text Label 2000 3200 2    50   ~ 0
Vline-
Text Label 4400 1400 1    50   ~ 0
S_2
Text Label 4500 1400 1    50   ~ 0
S_1
Text Label 4600 1400 1    50   ~ 0
S_0
Text Label 4400 2900 1    50   ~ 0
S_2
Text Label 4500 2900 1    50   ~ 0
S_1
Text Label 4600 2900 1    50   ~ 0
S_0
Text Label 4400 4400 1    50   ~ 0
S_2
Text Label 4500 4400 1    50   ~ 0
S_1
Text Label 4600 4400 1    50   ~ 0
S_0
Text Label 4400 5900 1    50   ~ 0
S_2
Text Label 4500 5900 1    50   ~ 0
S_1
Text Label 4600 5900 1    50   ~ 0
S_0
Text Label 2000 1800 0    50   ~ 0
S_2
Text Label 2000 1700 0    50   ~ 0
S_1
Text Label 2000 1600 0    50   ~ 0
S_0
Text HLabel 4200 1400 0    50   Input ~ 0
S_A
Text HLabel 4200 2900 0    50   Input ~ 0
S_B
Text HLabel 4200 4400 0    50   Input ~ 0
S_C
Text HLabel 4200 5900 0    50   Input ~ 0
S_D
Text HLabel 2000 1600 0    50   Input ~ 0
S_0
Text HLabel 2000 1700 0    50   Input ~ 0
S_1
Text HLabel 2000 1800 0    50   Input ~ 0
S_2
Text HLabel 2000 2000 2    50   Input ~ 0
Out
Text Label 4800 1400 0    50   ~ 0
Out
Text Label 2000 5500 2    50   ~ 0
~S~|H
Text Label 2500 5400 0    50   ~ 0
~S~|H
Text Label 2000 1400 0    50   ~ 0
~S~|H
Text HLabel 2000 1400 0    50   Input ~ 0
~S~|H
$Comp
L power:+8V #PWR0121
U 1 1 5DDC0C20
P 2000 3000
F 0 "#PWR0121" H 2000 2850 50  0001 C CNN
F 1 "+8V" V 2000 3100 50  0000 L CNN
F 2 "" H 2000 3000 50  0001 C CNN
F 3 "" H 2000 3000 50  0001 C CNN
	1    2000 3000
	0    1    1    0   
$EndComp
$Comp
L power:-8V #PWR03
U 1 1 5DE35BE6
P 2000 3200
F 0 "#PWR03" H 2000 3300 50  0001 C CNN
F 1 "-8V" V 2000 3300 50  0000 L CNN
F 2 "" H 2000 3200 50  0001 C CNN
F 3 "" H 2000 3200 50  0001 C CNN
	1    2000 3200
	0    1    1    0   
$EndComp
$EndSCHEMATC
