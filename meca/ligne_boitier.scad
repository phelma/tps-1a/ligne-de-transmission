include <ligne.scad>;

// Needs https://github.com/bmsleight/lasercut
include <lasercut.scad>;

// How to :
// * F6 pour compiler
// * Fichier > Exporter en SVG
// * Sous Inkscape : 
// * glisser-déposer ou ouvrir, 
// * sélectionner l'objet et faire Chemin > Séparer (ctrl-maj-K).
// * Retirer les remplissages


$fn=30;

// Parameters
mode = 1;
thick = 5;
drill_diam = 0;

coax_pos_z = 20;
ligne_pos_z = coax_pos_z + 6.9;

x_int = ligne_et_mesure_length + 1; // ligne_dims[0];
y_int = ligne_dims[1];
z_int = -thick + ligne_pos_z + 21.2;

// Screw sizes
// Ligne -- 3d support -- PMMA = 25
// Mesure-- 3d support -- PMMA = 25

module lignepos() {
    render(convexity = 2)
    translate([-0.49, 0, ligne_pos_z]) ligne_et_mesure();
}

module banana() {
    translate([0, 0, 0.01])
    cylinder(d=10, h=4, $fn=6);
    translate([0, 0, 0.01])
    cylinder(d=10, h=0.6);
    translate([0, 0, -thick-0.1]) cylinder(d=8, h=thick+0.2, $fn=90);
    translate([0, 0, -thick-6.3-0.01]) cylinder(d=10, h=6.3);
    translate([0, 0, -thick-6.3-2]) cylinder(d=9, h=2, $fn=6);
    translate([0, 0, -18]) cylinder(d=6, h=18);
}

module top() {
    difference() {
        translate([-x_int/2, -y_int/2, z_int+thick])
        lasercutoutSquare(thickness=thick, x=x_int, y=y_int,
            finger_joints=[
                [UP, 1, 4],
                [DOWN, 1, 4],
                [LEFT, 0, 4],
                [RIGHT, 0, 4],
            ],
            simple_tabs = [
                [DOWN, x_int+thick/2, 0],
                [UP, -thick/2, y_int],
            ],
            milling_bit = drill_diam
        );
    }
}

module bottom() {
    difference() {
        render(convexity = 2)
        translate([x_int/2, -y_int/2, thick])
        rotate([0, -180, 0])
        lasercutoutSquare(thickness=thick, x=x_int, y=y_int,
            finger_joints=[
                [UP, 1, 4],
                [DOWN, 1, 4],
                [LEFT, 0, 4],
                [RIGHT, 0, 4],
            ],
            simple_tabs = [
                [DOWN, x_int+thick/2, 0],
                [UP, -thick/2, y_int],
            ],
            milling_bit = drill_diam
        );

        // mounting points
        lignepos();
    }
}

module left() {
    difference() {
        render(convexity = 2)
        translate([-x_int/2, -y_int/2, thick])
        rotate([0, -90, 0])
        lasercutoutSquare(thickness=thick, x=z_int, y=y_int,
            finger_joints=[
                [UP, 1, 3],
                [DOWN, 1, 3],
                [LEFT, 0, 4],
                [RIGHT, 0, 4],
            ],
            simple_tabs = [
                [DOWN, z_int+thick/2, 0],
                [UP, -thick/2, y_int],
            ],
            milling_bit = drill_diam
        );
        
        // Coax connectors
        lignepos();
    }
}

module right() {
    difference() {
        render(convexity = 2)
        translate([x_int/2, -y_int/2, z_int + thick])
        rotate([0, 90, 0])
        lasercutoutSquare(thickness=thick, x=z_int, y=y_int,
            finger_joints=[
                [UP, 1, 3],
                [DOWN, 1, 3],
                [LEFT, 0, 4],
                [RIGHT, 0, 4],
            ],
            simple_tabs = [
                [DOWN, z_int+thick/2, 0],
                [UP, -thick/2, y_int],
            ],
            milling_bit = drill_diam
        );
        
        // Line connector
        lignepos();
    }
}

module front() {
    difference() {
        render(convexity = 2)
        translate([-x_int/2, -y_int/2, thick])
        rotate([90, 0, 0])
        lasercutoutSquare(thickness=thick, x=x_int, y=z_int,
            finger_joints=[
                [UP, 1, 4],
                [DOWN, 1, 4],
                [LEFT, 1, 3],
                [RIGHT, 1, 3],
            ],
            milling_bit = drill_diam
        );
        
        // Banana test points
        for(i=[1:6]) translate([(i-3.5)*44, -y_int/2-thick, 15]) {
            rotate([90, 0, 0]) banana();
        }
    }
}

module back() {
    difference() {
        render(convexity = 2)
        translate([-x_int/2, y_int/2, z_int+thick])
        rotate([-90, 0, 0])
        lasercutoutSquare(thickness=thick, x=x_int, y=z_int,
            finger_joints=[
                [UP, 1, 4],
                [DOWN, 1, 4],
                [LEFT, 1, 3],
                [RIGHT, 1, 3],
            ],
                milling_bit = drill_diam
        );

        // Power supply
        for(i=[-1:1]) translate([0+i*25, y_int/2+thick, 15]) {
            rotate([-90, 0, 0]) banana();
        }

        // USB connector
        translate([124, y_int/2-1, ligne_pos_z + 13])
            cube_c([14, thick+2, 10], [0, 1, 0]);
    }
}



module box() {
    //color("Blue",0.8)   top();
    //color("Blue",0.8)   bottom();

    color("Red",0.8)    left();
    color("Red",0.8)    right();

    color("Yellow",0.8) front();
    color("Yellow",0.8) back();

    %lignepos();
}

module to_dxf() {
    offset_2d = 3 + 2*thick;
    
    translate([x_int/2 + thick, 0, 0]) {
        translate([0, 1*y_int/2 + thick, -z_int-thick]) top();
        translate([0, 3*y_int/2 + thick + offset_2d, thick]) rotate([180, 0]) bottom();
    }
    translate([x_int/2 + thick, 2*y_int + 2*offset_2d]) {
        translate([0, 0, 0]) 
            rotate([-90, 0]) translate([0, y_int/2])front();
        translate([0, z_int + offset_2d, 0]) 
            rotate([90, 0, 180]) translate([0, -y_int/2]) back();
    }
    translate([0 + thick, 2*y_int + 2*z_int + 4*offset_2d, -thick/2]) {
        translate([0, 0, 0]) 
            rotate([0, 90, 90]) translate([x_int/2,-y_int/2]) left();
        translate([y_int + 3*thick, 0, 0]) 
            rotate([0, -90, -90]) translate([-x_int/2, y_int/2]) right();
    }
}

if (mode == 0) box(); 
if (mode == 1) to_dxf();
if (mode == 2) projection(cut=true) to_dxf();
