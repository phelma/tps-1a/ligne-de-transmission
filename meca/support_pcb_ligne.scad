$fn=30;

box_length = 85-0.12;

pcb_length = 85;
inter_screw_length = 75;

conn_height = 10;

support_width = 2*4.5;

support_height = 20+6.9-5;
echo(support_height);


intermediary_thick = 2;
intermediary_height = support_height - conn_height - intermediary_thick;

module cube_c(dimensions = [1, 1, 1], align = [1, 1, 1]) {
    for (a=align)
        assert(a == 0 || a == 1 || a == -1);
    translate([ for (i=[0, 1, 2]) dimensions[i] * align[i] / 2 ])
    cube(dimensions, center=true);
}

module pcb_line_support() {
    difference() {
    union() {
        for (i=[-1, 1]) translate([0, i*inter_screw_length/2, 0]) {
            cube_c([support_width, support_width, support_height], [0, 0, 1]);
        }
        // intermediary support
        for (i=[-1, 1]) translate([0, i*inter_screw_length*1/6, 0]) {
            cube_c([support_width, 1, intermediary_height], [0, 0, 1]);
            cylinder(d=support_width, h=0.4);
        }
        translate([0, 0, intermediary_height])
        cube_c([support_width, inter_screw_length, intermediary_thick], [0, 0, 1]);
    }
    union() {
        // screw holes
        for (i=[-1, 1]) translate([0, i*inter_screw_length/2, -1]) {
            cylinder(d=3.5, h=support_height + 3);
        }
        
    }
    }
}

pcb_line_support();
