$fn=30;

box_length = 85-0.2;

pcb_length = 60+0.4;
pcb_thick = 2;

inter_pin_width = 12;

support_height = 20.1;
echo(support_height);


module cube_c(dimensions = [1, 1, 1], align = [1, 1, 1]) {
    for (a=align)
        assert(a == 0 || a == 1 || a == -1);
    translate([ for (i=[0, 1, 2]) dimensions[i] * align[i] / 2 ])
    cube(dimensions, center=true);
}

module pcb_mesure_support() {
    difference() {
        cube_c([inter_pin_width, box_length, support_height + pcb_thick+1], [0, 0, 1]);
        
        translate([0, 0, support_height]) {
            cube_c([inter_pin_width+1, pcb_length, pcb_thick+0.01], [0, 0, 1]);
            cube_c([inter_pin_width+1, pcb_length-1, pcb_thick+1+0.01], [0, 0, 1]);
        }
        
        translate([0, 0, -0.01])
        cube_c([inter_pin_width+1, pcb_length, support_height*3/4], [0, 0, 1]);
        
        for (i=[-1, 1]) translate([0, i*(box_length+pcb_length)/2/2, -1])
        cylinder(d=3.5, h=support_height + pcb_thick + 3);
    }
}

pcb_mesure_support();
