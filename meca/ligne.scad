$fn=100;

include <support_pcb_ligne.scad>;
include <support_pcb_mesure.scad>;

ligne_dims = [
    202,
    85,
    1.8
];
ligne_components_top = [
    165,
    65,
    5
];
ligne_mountingpoints = [
    192,
    75
];
coax_pos_y = [
    28,
    57
];


mesure_dims = [
    75,
    60,
    1.8,
];

ligne_et_mesure_length = ligne_dims[0]+7.1+mesure_dims[0];

// colors
module black() { color("DarkSlateGray", 0.8) children(); }
module pcb()   { color("DarkGreen", 0.8) children(); }
module grey()  { color("Grey", 0.8) children(); }

module cube_c(dimensions = [1, 1, 1], align = [1, 1, 1]) {
    for (a=align)
        assert(a == 0 || a == 1 || a == -1);
    translate([ for (i=[0, 1, 2]) dimensions[i] * align[i] / 2 ])
    cube(dimensions, center=true);
}

module b6252h7() {
    body_height = 13.1;
    body_width  = 14.5;
    
    fillet_length = 8.9;
    fillet_diam = 12.83;
    
    conn_length = 12;
    conn_diam = 9.5;
    body_length = 34.8 - fillet_length - conn_length;
    conn_pos_z = 6.9;
    cube_c([body_length, body_height, body_height], [-1, 0, 1]);
    
    translate([0, 0, conn_pos_z])
    intersection() {
        translate([0, 0, -fillet_diam/2])
            cube_c([fillet_length, fillet_diam, 11.7], [1, 0, 1]);
        rotate([0, 90, 0]) cylinder(d=fillet_diam, h=fillet_length);
    }
    translate([fillet_length, 0, conn_pos_z]) 
        rotate([0, 90, 0]) cylinder(d=conn_diam, h=conn_length);
}

module conn_ligne() {
    lines = 2;
    rows = 20;
    y_margin = (7.62+0.3)/2;
    int_y = 2.54 * rows + 5.32;
    ext_y = 2.54 * rows + 2 * y_margin;
    x_margin = (8.8 - 2.54)/2;
    int_x = 6.5;
    ext_x = 8.8;

    translate([-9, -ext_y/2 + y_margin, 0])
    for(i=[0 : rows]) {
        translate([-1.5, i*2.54, -3]) {
            cube_c([0.64, 0.64, 3+x_margin+0.64/2], [0, 0, 1]);
            translate([0, 0, 3+x_margin])
            cube_c([9+1.5, 0.64, 0.64], [1, 0, 0]);
        }
        translate([-1.5-2.54, i*2.54, -3]) {
            cube_c([0.64, 0.64, 3+8.8-x_margin+0.64/2], [0, 0, 1]);
            translate([0, 0, 3+8.8-x_margin])
            cube_c([9+1.5+2.54, 0.64, 0.64], [1, 0, 0]);
        }
    }

    difference() {
        cube_c([9, ext_y, ext_x], [-1, 0, 1]);
        translate([0.01, 0, (ext_x-int_x)/2])
        cube_c([8, int_y, int_x], [-1, 0, 1]);
    }

}

module conn_ligne() {
    rows = 20;
    y_margin = (7.62+0.3)/2;
    ext_y = 2.54 * rows + 2 * y_margin;
    translate([2.54+1.5, 0])
        cube_c([9, ext_y, 8.95], [1, 0, 1]);
}
module conn_mesure() {
    rows = 20;
    ext_y = 2.54 * rows + 0.51;
    ext_x = 4.95;
    cube_c([8.51, ext_y, ext_x], [-1, 0, 1]);
}

module mountingholes() {
    l=30;
    for (i=[0, 1]) for (j=[-1, 1])
    translate([-i*ligne_dims[0] + (2*i-1)*5, j*ligne_mountingpoints[1]/2, -l+2]){
        cylinder(d=2.3, h=l);
    }
}


module ligne() {
    pcb() cube_c(ligne_dims, [-1, 0, 1]);
    translate([-ligne_dims[0]/2, 0, ligne_dims[2]])
        cube_c(ligne_components_top, [0, 0, 1]);

    for(y = coax_pos_y) {
        translate([-ligne_dims[0], -ligne_dims[1]/2+y, 0])
        rotate([0, 180, 0])
        grey() b6252h7();
    }

    translate([-6, 0, 0])
    rotate([180, 0, 0])
    black() conn_ligne();
    
    mountingholes();
}

module mesure() {
    pcb() {
        cube_c([5, 51.4, mesure_dims[2]], [1, 0, 1]);
        translate([5, 0, 0]) cube_c(mesure_dims-[5, 0, 0], [1, 0, 1]);
    }
    
    // Arduino
    translate([48, 14-mesure_dims[1]/2, mesure_dims[2]]) {
        cube_c([18, 49, 13]);
        cube_c([8, 10, 21]);
    }
    
    // DC connector
    translate([26, 31-mesure_dims[1]/2, 0])
        grey() cube_c([15.5, 11, 13], [1, 1, -1]);
    
    // line connector
    translate([-5.8, 0, 0])
    rotate([0, 180, 0])
    grey() conn_mesure();
}

module ligne_et_support() {
    ligne();
    translate([-5, 0, -20]) {
        pcb_line_support();
    }
}
module mesure_et_support() {
    mesure();
    translate([48 + 18/2, 0, -support_height+0.01]) {
        pcb_mesure_support();
        for (i=[-1, 1]) translate([0, i*(box_length+pcb_length)/2/2, -10])
        cylinder(d=2.3, h=support_height + pcb_thick + 3);
    }
    
}

module ligne_et_mesure() {
    translate([ligne_dims[0]-ligne_et_mesure_length/2, 0]) {
        ligne_et_support();
        translate([7.1, 0, -1.8]) mesure_et_support();
    }
}

// ligne_et_mesure();
