
class FakeMeasurer {
public:
  FakeMeasurer() {
    randomSeed(analogRead(5));
  };
  
  ~FakeMeasurer() = default;

  void take_measure() {
      rand_value = random(-100, 100) / 100.0;
  }

  int read_input(int input) {
    return sin(input/5.0)*50*rand_value;
  };

private:
  double rand_value;

};
