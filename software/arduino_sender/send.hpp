
class Sender {
public:
  Sender() {
    // Initialize Serial port
    Serial.begin(250000);
    // while (!Serial){};
  };
  ~Sender() = default;

  void send(const int values[], int count) {
    line.reserve(200);
    line = "{\"len\":";
    line += String(count);
    line += ",\"val\": [";
    for (int i = 0; i < count; ++i) {
      line += String(values[i]);
      line += ",";
    }
    line[line.length() - 1] = ']'; // Remove last ',' that is not json compliant
  
    line += ("}\n");
    Serial.print(line);
  }
private:
  String line;
};
