
__attribute__((always_inline, unused))
static inline void delayNanoseconds(uint32_t nsec) {
    /*
     * Based on Paul Stoffregen's implementation
     * for Teensy 3.0 (http://www.pjrc.com/)
     */
    if (nsec == 0) return;
    uint32_t n = (nsec * 1000) / 35714;
    asm volatile(
        "L_%=_delayNanos:"       "\n\t"
        "subs   %0, #1"                 "\n\t"
        "bne    L_%=_delayNanos" "\n"
        : "+r" (n) :
    );
}

class Measurer {
public:
  Measurer() {
    pinMode(sample_holder, OUTPUT);
    digitalWrite(sample_holder, HIGH);

    pinMode(address0, OUTPUT);
    pinMode(address1, OUTPUT);
    pinMode(address2, OUTPUT);

    pinMode(select_1, OUTPUT);
    pinMode(select_2, OUTPUT);
    pinMode(select_3, OUTPUT);
    pinMode(select_4, OUTPUT);
    
    pinMode(input_ref, INPUT);
    pinMode(input_measure, INPUT);
  };

  ~Measurer() = default;

  void take_measure() {
    digitalWrite(sample_holder, LOW);
    // delayNanoseconds(200);
    //delay(0);
    digitalWrite(sample_holder, HIGH);
  }

  int read_input(int input) {
    select_input(input);
    int value = analogRead(input_measure);
    
    int real_value = value - reference();    
    return real_value;
  };

private:
  // Latch for sample holders
  const byte sample_holder = 4;

  // 3 first bytes of input number
  const byte address0 = 5;
  const byte address1 = 6;
  const byte address2 = 7;

  // 4th and 5th bytes of input number (select the demux)
  const byte select_1 = 11;
  const byte select_2 = 10;
  const byte select_3 = 9;
  const byte select_4 = 8;

  // Analog input
  const byte input_ref = A0;
  const byte input_measure = A1;
  
  int reference_value = 3;


  void select_input(int input) {
    digitalWrite(select_1, HIGH);
    digitalWrite(select_2, HIGH);
    digitalWrite(select_3, HIGH);
    digitalWrite(select_4, HIGH);

    digitalWrite(address0, (input & 1) ? HIGH : LOW);  // low-order bit
    digitalWrite(address1, (input & 2) ? HIGH : LOW);
    digitalWrite(address2, (input & 4) ? HIGH : LOW);  // high-order bit

    // Enable pin (active low)
    digitalWrite(select_1, ((input>>3) == 0) ? LOW : HIGH);
    digitalWrite(select_2, ((input>>3) == 1) ? LOW : HIGH);
    digitalWrite(select_3, ((input>>3) == 2) ? LOW : HIGH);
    digitalWrite(select_4, ((input>>3) == 3) ? LOW : HIGH);
  };

  int reference() {
    if (reference_value < 100)
      reference_value = analogRead(input_ref);
    return reference_value;
  };

};
