#include "send.hpp"
// #include "fake_measure.hpp"
#include "measure.hpp"


Sender* sender;
Measurer* measurer;

void setup() {
  sender = new Sender();
  measurer = new Measurer();
  delay(100);
}

const int length = 62;
int values[length];

void loop() {
  measurer->take_measure();
  
  for (int i = 0; i < length; ++i) {
    values[i] = measurer->read_input(i);
  }

  sender->send(values, length);

  //delay(1);
}
