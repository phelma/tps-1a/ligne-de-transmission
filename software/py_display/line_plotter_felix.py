#!/usr/bin/env python3

import os, sys, signal
import queue
import tempfile
from shutil import copyfile
import csv

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk, Gdk

import matplotlib as mpl
mpl.use('GTK3Cairo')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigCanvas

import numpy as np

import serial_read
import customization

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="org.phelma.tp_1a_ligne",
             flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
             **kwargs
        )
        self.add_main_option('fake', ord('f'),
            GLib.OptionFlags.NONE, GLib.OptionArg.NONE, 'Use fake data instead of serial port', None
        )

        self.window = None
        self.queue = queue.Queue()
        self.serial = serial_read.SerialRead(self.queue)

    def do_startup(self):
        Gtk.Application.do_startup(self)
        GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, signal.SIGINT, self.quit)

    def do_activate(self):
        if not self.window:
            self.window = PlotWindow(application=self)

        self.window.present()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        options = options.end().unpack()


        self.activate()
        return 0

class Pauser():
    def __init__(self, plotwindow):
        self.pw = plotwindow
    def __enter__(self):
        self.was_running = self.pw.running
        self.pw.start_stop_acquisition(False)
    def __exit__(self, type, value, traceback):
        if self.was_running: self.pw.start_stop_acquisition(True)


class PlotWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.application = kwargs['application']
        self.setup_graph()
        self.setup_window()

        self.connect('delete-event', self.on_destroy)

        self.screenshot_filename = ''
        self.save_data_filename = ''

        # max_action = Gio.SimpleAction.new_stateful("maximize", None,
        #     GLib.Variant.new_boolean(True)
        # )
        # max_action.connect("change-state", self.on_maximize_toggle)

    def on_destroy(self, widget=None, *data):
        if self.running:
            self.application.serial.stop()
        return False

    def setup_window(self):
        self.set_default_size(800, 600)
        self.set_title("Simulation de ligne de transmission RF")
        self.set_icon_from_file(os.path.join(__location__, 'line_plotter.svg'))

        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(__location__, 'line_plotter.glade'))
        container = builder.get_object('top_level_container')

        self.port_combo = builder.get_object('port_combo')

        self.start_stop_button = builder.get_object('start_button')
        self.start_stop_icon = builder.get_object('start_button_icon')

        # Configure combo
        self.port_combo.set_id_column(0)
        renderer_text = Gtk.CellRendererText()
        self.port_combo.pack_start(renderer_text, True)
        self.port_combo.add_attribute(renderer_text, "text", 0)

        self.running = False
        self.curve_count = 20

        builder.get_object('adjustment1').set_value(self.curve_count)

        # Configure other widgets
        self.update_ports(None)
        self.select_port(self.port_combo)

        container.pack_end(self.canvas, True, True, 0)

        self.add(container)
        self.show_all()

        # Connect signals (from widgets) to functions
        builder.connect_signals({
            'update_ports': self.update_ports,
            'port_selected': self.select_port,
            'curve_count_changed': self.curve_count_change,
            'start_button_clicked': self.start_stop_acquisition_callback,
            'screenshot_button_clicked': self.screenshot_plot,
            'save_data_button_clicked': self.save_data,
        })

        # Connect accelerators (shortcuts) to functions
        accelerators = Gtk.AccelGroup()
        self.add_accel_group(accelerators)

        key, mod = Gtk.accelerator_parse("<Control>P")
        accelerators.connect(key, mod, Gtk.AccelFlags.VISIBLE, self.ctrl_p)

        key, mod = Gtk.accelerator_parse("<Control>S")
        accelerators.connect(key, mod, Gtk.AccelFlags.VISIBLE, self.ctrl_s)

    def ctrl_p(self, accel_group, acceleratable, keyval, modifier):
        self.screenshot_plot(None)

    def ctrl_s(self, accel_group, acceleratable, keyval, modifier):
        self.save_data(None)

    def update_ports(self, button):
        # Keep in memory the last value to keep the combo set after update
        last_iter = self.port_combo.get_active_iter()
        last_port = None
        if last_iter is not None:
            last_port = self.port_combo.get_model()[last_iter][0]

        store = self.port_combo.get_model()
        store.clear()
        new_active_iter = None
        for port in serial_read.SerialRead.list_ports():
            port_path = port.device
            iter = store.append([port_path])
            if port_path == last_port:
                new_active_iter = iter

        self.port_combo.set_active_iter(new_active_iter)

    def select_port(self, combo):
        iter = combo.get_active_iter()
        self.port = None
        if iter is not None:
            self.port = combo.get_model()[iter][0]

        self.update_start_stop_button()

    def update_start_stop_button(self):
        if self.running:
            self.start_stop_button.set_label("Arrêter l'acquisition")
            self.start_stop_icon.set_from_icon_name('gtk-media-pause', Gtk.IconSize.BUTTON)
        else:
            self.start_stop_button.set_label("Démarrer l'acquisition")
            self.start_stop_icon.set_from_icon_name('gtk-media-play', Gtk.IconSize.BUTTON)

        can_press_button = self.running or (self.port is not None)
        self.start_stop_button.set_sensitive(can_press_button)

    def curve_count_change(self, spinner):
        self.curve_count = spinner.get_value()

    def start_stop_acquisition_callback(self, button):
        self.start_stop_acquisition(not self.running)

    def start_stop_acquisition(self, start):
        if start:
            self.application.serial.serial_port = self.port
            self.application.serial.start()
            self.anim.event_source.start()
        else:
            self.application.serial.stop()
            self.anim.event_source.stop()

        self.running = start
        self.update_start_stop_button()

    def screenshot_plot(self, button):
        with Pauser(self):
            dialog = Gtk.FileChooserDialog(title="Enregistrer le graphe",
                action=Gtk.FileChooserAction.SAVE
            )
            dialog.add_buttons(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE,   Gtk.ResponseType.OK
            )

            if self.screenshot_filename != '':
                dialog.set_current_name(self.screenshot_filename)
            else:
                dialog.set_current_name('screenshot.png')


            filter_png = Gtk.FileFilter()
            filter_png.set_name('Image PNG')
            filter_png.add_mime_type('image/png')
            dialog.add_filter(filter_png)

            response = dialog.run()
            if response == Gtk.ResponseType.OK:
                self.screenshot_filename = dialog.get_filename()
                if not self.screenshot_filename.endswith('.png'):
                    self.screenshot_filename += '.png'

                plt.savefig(self.screenshot_filename)
                # Seek to be sure writing to disk finished
                with open(self.screenshot_filename, 'r') as f: f.seek(0)

            dialog.destroy()

    def write_data_envelope(self, filename):
        envelope_data_min = [ -i for i in range(0, 31) ]
        envelope_data_max = [ i*i for i in range(31) ]

        all_lines = [ i.get_ydata() for i in self.lines ]
        envelope_data_min = np.minimum.reduce(all_lines)
        envelope_data_max = np.maximum.reduce(all_lines)

        with open(filename, 'w') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['cellule', 'minimum', 'maximum'])
            for i in range(0, 31):
                writer.writerow([i, envelope_data_min[i], envelope_data_max[i]])

        return

    def save_data(self, button):
        with Pauser(self):

            if len(self.lines) == 0:
                dialog = Gtk.MessageDialog(
                    parent=self,
                    flags=0,
                    message_type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.CANCEL,
                    text="Aucune donnée à enregistrer"
                )
                dialog.format_secondary_text("Démarrez l'acquisition pour avoir des données !")
                dialog.run()
                dialog.destroy()
                return

            dialog = Gtk.FileChooserDialog(title="Enregistrer les données de l'enveloppe",
                action=Gtk.FileChooserAction.SAVE
            )
            dialog.add_buttons(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE,   Gtk.ResponseType.OK
            )

            if self.save_data_filename != '':
                dialog.set_current_name(self.save_data_filename)
            else:
                dialog.set_current_name('data_enveloppe.csv')

            filter_csv = Gtk.FileFilter()
            filter_csv.set_name('Fichier CSV')
            filter_csv.add_mime_type('text/csv')
            dialog.add_filter(filter_csv)

            response = dialog.run()
            if response == Gtk.ResponseType.OK:
                self.save_data_filename = dialog.get_filename()
                if not self.save_data_filename.endswith('.csv'):
                    self.save_data_filename += '.csv'

                self.write_data_envelope(self.save_data_filename)

            dialog.destroy()


    def setup_graph(self):
        self.cells_count = 32
        self.cells_range = range(0, self.cells_count)

        fig = mpl.pyplot.figure(figsize=(8, 8))
        # Add a subplot with no frame
        self.ax = plt.subplot(111)
        self.ax.set_xlim(0, self.cells_count-1)
        self.ax.set_ylim(-0.5, 0.5)

        self.canvas = FigCanvas(fig)
        self.canvas.set_size_request(800, 600)

        mpl.rcParams['path.simplify'] = True
        mpl.rcParams['path.simplify_threshold'] = 1.0

        major_ticks = np.arange(0, self.cells_count-1, 2)
        minor_ticks = np.arange(-0.5, 0.5, 0.02)

        #self.ax.set_xticks(major_ticks)
        ##self.ax.set_xticks(minor_ticks, minor=True)
        # self.ax.set_yticks(major_ticks)
        self.ax.set_yticks(minor_ticks, minor=True)

        # And a corresponding grid
        self.ax.grid(which='both')

        # Or if you want different settings for the grids:
        self.ax.grid(which='major', alpha=0.5)
        self.ax.grid(which='minor', alpha=0.2, linestyle=':')


        # Construct the animation, using the update function as animation director
        self.anim = animation.FuncAnimation(
            fig,
            self.update_graph,
            interval=16,
            blit=False
        )
        self.anim.event_source.stop()
        self.lines = []
        self.lines_x = list(range(0, self.cells_count))

    def update_graph(self, *args):
        if self.running:
            # First get all from queue
            new_lines = []
            while not self.application.queue.empty():
                line = self.application.queue.get(False)
                if line is not None :
                    new_lines.append(line)
            self.application.queue.task_done()

            if len(new_lines) > 0:
                # Corrections
                for line in new_lines:
                    customization.data_correction(line)

                # Liste de lignes  ——> liste de colonnes
                cells = [ [ line[i] for line in new_lines ]
                    for i in range(0, self.cells_count)
                ]

                lines = self.ax.plot(self.lines_x, cells)
                # print(lines)
                self.lines.extend(lines)

            while len(self.lines) > self.curve_count:
                self.lines[0].remove()
                self.lines = self.lines[1:]

            return self.lines


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
