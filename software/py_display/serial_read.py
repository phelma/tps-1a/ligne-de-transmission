#!/usr/bin/env python3

import threading
import serial
from serial.tools import list_ports
import json

import time

class ReadLine:
    def __init__(self, s):
        self.buf = bytearray()
        self.s = s

    def readline(self):
        i = self.buf.find(b"\n")
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            return r
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            if data == b'': return None
            i = data.find(b"\n")
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                return r
            else:
                self.buf.extend(data)


class SerialRead():
    """docstring for SerialRead."""
    def __init__(self, queue):
        self.queue = queue
        self.cancel = True
        self.serial_thread = None

        # self.serial_port = serial.tools.list_ports.comports(False)[1]
        self.serial_port = None

        self.serial_conn = None

    def start(self):
        if not self.cancel: return
        self.cancel = False
        self.serial_thread = threading.Thread(target=self.run, args=())
        self.serial_thread.start()

    def stop(self):
        if self.cancel: return
        self.cancel = True
        self.serial_thread.join()

    def run(self):
        self.connect_wait()
        for line in self.read_serial_lines():
            data = self.parse_line(line)
            if data is not None:
                self.queue.put(data)
        self.disconnect()

    def list_ports():
        return list_ports.comports()

    def connect_wait(self):
        print('Connecting to', self.serial_port)
        while not self.cancel:
            try:
                if self.serial_port is None:
                    continue
                self.serial_conn = serial.Serial(
                    port=self.serial_port,
                    baudrate=115200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=0.1,
                    inter_byte_timeout=0.5
                )
                self.readliner = ReadLine(self.serial_conn)
                print('Connected to', self.serial_port)
                break
            except Exception as e:
                print('Could not connect: ', e)
                time.sleep(0.3)


    def disconnect(self):
        if self.serial_conn:
            self.readliner = None
            self.serial_conn.close()
            self.serial_conn = None


    def read_serial_lines(self):
        while not self.cancel:
            try:
                if self.serial_conn == None:
                    self.connect()
                    print("Reconnected")

                line = self.readliner.readline()
                if line is not None: yield line

            except:
                print("Disconnected!")
                if not(self.serial_conn == None):
                    self.disconnect()
                time.sleep(0.300)


    def parse_line(self, line):
        # print(line)
        try:
            json_data = json.loads(line)
        except json.decoder.JSONDecodeError as e:
            print(e)
            print(line)
            return None

        length = json_data['len']
        values = json_data['val']
        if len(values) != length:
            print('Warning: length=%d but %d values retrieved!' % (length, len(values)))
            # print(text_data)
            return None

        # Convert to volts
        values_volt = [ i/1024 for i in values ]
        return (values_volt)
