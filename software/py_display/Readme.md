pgtk	pipG# TP Ligne de transmission RF : Logiciel d'acquisition

Ce logiciel est écrit en Python. Il a donc besoin de l'interpréteur, ainsi que des
librairies utilisées :

* `GTK` / `Gobject`
* `matplotlib`
* `pyserial`
* `python-json`

# Linux
Tous ces outils/librairies sont disponibles dans le gestionnaire de paquets ou dans Pip.

## Linux Mint / Ubuntu
```
sudo apt update && sudo apt install python3-serial python3-matplotlib python3-json5 python3-gi-cairo

sudo
sudo usermod -a -G dialout $USER
sudo usermod -a -G tty $USER
sudo chmod a+rw /dev/ttyACM0


# Vous devez fermer et rouvrir la session après usermod !

git clone https://gricad-gitlab.univ-grenoble-alpes.fr/phelma/tps-1a/ligne-de-transmission.git

# Générez le lanceur :

./ligne-de-transmission/software/py_display/generate_desktop_file.py

```

## CentOS
```
sudo yum install -y epel-release
sudo yum install -y git python3 python36-gobject
sudo pip3 install matplotlib cairocffi pyserial
sudo usermod -a -G dialout $USER

# Vous devez fermer et rouvrir la session après usermod !

git clone https://gricad-gitlab.univ-grenoble-alpes.fr/phelma/tps-1a/ligne-de-transmission.git

```

# Windows
Le logiciel tourne sous Linux exclusivement.
