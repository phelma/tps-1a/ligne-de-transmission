#!/usr/bin/env python3

import os
import stat
from pathlib import Path
import subprocess


desktop_file_string = '''[Desktop Entry]
Name=Afficheur TP Ligne
Encoding=UTF-8
Exec=python3 {}
Icon={}
Terminal=false
Type=Application
StartupNotify=false
Categories=Education;Science
'''


def generate_desktop_file():
    desktop_file_path = Path(
        subprocess.check_output(['xdg-user-dir', 'DESKTOP']).decode('UTF-8').rstrip(),
        'LinePlotter.desktop'
    )


    install_dir = os.path.dirname(os.path.realpath(__file__))

    with open(desktop_file_path, 'w') as desktopfile:
        desktopfile.write(desktop_file_string.format(
            Path(install_dir, 'line_plotter.py'),
            Path(install_dir, 'line_plotter.svg')
        ))

    desktop_file_path.chmod(desktop_file_path.stat().st_mode | stat.S_IEXEC)


if __name__ == "__main__": generate_desktop_file()
